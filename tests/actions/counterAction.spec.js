import expect from 'expect';
import reducers from '../../app/reducers/reducers';
import { createStore } from 'redux';
import { INCREMENT, DECREMENT } from '../../app/actions/counterAction';

describe('Counter Action', () => { 
    let store = createStore(reducers);

    it('should increment the count value in state to 1', () => { 
        store.dispatch({ 
            type: INCREMENT
        });

        let actual = store.getState().counter.count;
        let expected = 1;
        expect(expected).toEqual(actual);
    });

    it('should decrement the count value in state to 0', () => { 
        store.dispatch({ 
            type: DECREMENT
        });

        let actual = store.getState().counter.count;
        let expected = 0;

        expect(expected).toEqual(actual);
    });

    it('should not decrement the count value in state to lower than 0', () => { 
        store.dispatch({ 
            type: DECREMENT
        });

        store.dispatch({ 
            type: DECREMENT
        });

        let actual = store.getState().counter.count;
        let expected = 0;

        expect(expected).toEqual(actual);
    });

});

