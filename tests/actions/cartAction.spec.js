import expect from 'expect';
import reducers from '../../app/reducers/reducers';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { STATE_NAME } from '../../app/constants/constants';
import { removeFromShoppingCart } from '../../app/actions/cartAction';

describe('Cart Action API', () => { 
    let store = applyMiddleware(thunk)(createStore)(reducers);

    afterEach(() => { 
        store.dispatch({ 
            type: 'RESET_CART'
        });
    });

    it('should add an item to shopping cart', () => { 
        let item = store.getState()[STATE_NAME.SESSION_MERCHANDIZE].sections[0].inputs[0];

        store.dispatch({ 
            type: 'ADD_TO_CART',
            item,
            quantity: 5
        });

        let actual = store.getState()[STATE_NAME.CART].cartItems.length;
        let expected = 1;

        expect(actual).toEqual(expected);

    });

    it('should update all cart items', () => { 
        let item = store.getState()[STATE_NAME.SESSION_MERCHANDIZE].sections[0].inputs[0];

        store.dispatch({ 
            type: 'ADD_TO_CART',
            item,
            quantity: 5
        });

        let actual = store.getState()[STATE_NAME.CART].cartItems.length;
        let expected = 1;

        expect(actual).toEqual(expected);

        store.dispatch({ 
            type: 'UPDATE_ALL_CART_ITEMS',
            newCart: {cartItems: [Object.assign({},{ ...item, response: 1 })]}
        });

        actual = store.getState()[STATE_NAME.CART].cartItems[0].response;
        expected = 1;

        expect(actual).toEqual(expected);

    });

    it('should remove an item from shopping cart', () => { 
        let item = store.getState()[STATE_NAME.SESSION_MERCHANDIZE].sections[0].inputs[0];

        store.dispatch({ 
            type: 'ADD_TO_CART',
            item,
            quantity: 5
        });

        store.dispatch({ 
            type: 'REMOVE_FROM_CART',
            item
        });

        let actual = store.getState()[STATE_NAME.CART].cartItems.length;
        let expected = 0;

        expect(actual).toEqual(expected);
    });

    it('should remove an item from shopping cart using redux thunk', () => { 
        let item = store.getState()[STATE_NAME.SESSION_MERCHANDIZE].sections[0].inputs[0];

        store.dispatch({ 
            type: 'ADD_TO_CART',
            item,
            quantity: 5
        });

        store.dispatch(removeFromShoppingCart(item));

        let actual = store.getState()[STATE_NAME.CART].cartItems.length;
        let expected = 0;

        expect(actual).toEqual(expected);
    });

    it('should has no side affect when remove an item that is not in the cart', () => { 
        let item = store.getState()[STATE_NAME.SESSION_MERCHANDIZE].sections[0].inputs[0];

        store.dispatch(removeFromShoppingCart(item));

        let actual = store.getState()[STATE_NAME.CART].cartItems.length;
        let expected = 0;

        expect(actual).toEqual(expected);
    });
});
