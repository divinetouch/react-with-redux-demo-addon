import expect from 'expect';
import cartReducer from '../../app/reducers/cartReducer';

describe('Cart Reducer', () => { 
    let item1 = { 
        id: 1,
        price: 40,
        name: 'item name 1',
        description: 'This is item number 1',
        quantity: 2
    };

    let item2 = { 
        id: 2,
        price: 50,
        name: 'item name 2',
        description: 'This is item number 2'
    };

    it('should return a proper state after adding a new item', () => { 
        let initialState = { 
            cartItems: [item1]
        };

        let action = { 
            type: 'ADD_TO_CART',
            item: item2,
            quantity: 3
        };

        let actual = cartReducer(initialState, action);
        let expected = {cartItems: [item1, Object.assign({}, { ...item2, quantity:3 })]};

        expect(actual).toEqual(expected);

    });

    it('should return a proper state after removing an item', () => { 
        let initialState = { 
            cartItems: [item1, item2]
        };

        let action = { 
            type: 'REMOVE_FROM_CART',
            item: item2
        };

        let actual = cartReducer(initialState, action);
        let expected = {cartItems: [item1]};

        expect(actual).toEqual(expected);

    });

    it('should remove a correct item', () => { 
        let initialState = { 
            cartItems: [item1, item2]
        };

        let action = { 
            type: 'REMOVE_FROM_CART',
            item: { 
                id: 3,
                name: 'Item name 3',
                description: 'This is item number 3',
                price: 3
            }
        };

        let actual = cartReducer(initialState, action);
        let expected = {cartItems: [item1, item2]};

        expect(actual).toEqual(expected);

    });

    it('should reset cart state', () => { 
        let initialState = { 
            cartItems: [item1, item2]
        };

        let action = { 
            type: 'UPDATE_ALL_CART_ITEMS',
            newCart: {
                cartItems: [{ 
                    id: 3,
                    name: 'Item name 3',
                    description: 'This is item number 3',
                    price: 3
                }]
            }
        };

        let actual = cartReducer(initialState, action);
        let expected = {
            cartItems: [{ 
                id: 3,
                name: 'Item name 3',
                description: 'This is item number 3',
                price: 3
            }]
        };

        expect(actual).toEqual(expected);
    });
});
