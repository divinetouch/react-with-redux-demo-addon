import expect from 'expect';
import { createStore } from 'redux';
import reducers from '../../app/reducers/reducers';
import { STATE_NAME } from '../../app/constants/constants';
import { testData as sessionTestData } from '../../app/testData/sesssionTestData';
import { testData as regTypeTestData } from '../../app/testData/regTypeTestData';
import { testData as routeTestData } from '../../app/testData/routeTestData';
import { testData as profileInputsTestData } from '../../app/testData/profileTestData';
import { testData as surveyInputsTestData } from '../../app/testData/surveyTestData';
import { testData as applicationStateTestData } from '../../app/testData/applicationStateTestData';

describe('Initial state', () => { 
    let store = createStore(reducers);
    it('should initialize', () => { 
        let actual = store.getState();
        let expected = { 
            [STATE_NAME.CART]: { cartItems: [] },
            [STATE_NAME.SESSION_MERCHANDIZE]: sessionTestData,
            [STATE_NAME.REGTYPE_MERCHANDIZE]: regTypeTestData,
            [STATE_NAME.PROFILE_INPUTS]: profileInputsTestData,
            [STATE_NAME.SURVEY_INPUTS]: surveyInputsTestData,
            [STATE_NAME.APPLICATION_ROUTE]: routeTestData,
            [STATE_NAME.APPLICATION_STATE]: applicationStateTestData,
            [STATE_NAME.COUNTER]: { 
                count: 0
            },
            routing: { 
                changeId: 1,
                path: undefined,
                replace: false,
                state: undefined
            }
        };

        expect(actual).toEqual(expected);
    });
});
