import React from 'react';
import ReactDOM from 'react-dom';
import Input from '../../../app/components/inputs/input';
import TestUtils from 'react-addons-test-utils';
import expect from 'expect';
import expectJSX from 'expect-jsx';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import reducers from '../../../app/reducers/reducers';
import { STATE_NAME } from '../../../app/constants/constants';
expect.extend(expectJSX);

describe('Input component', () => { 
    let store = applyMiddleware(thunk)(createStore)(reducers);
    let input = store.getState()[STATE_NAME.PROFILE_INPUTS].sections[0].inputs[0];

    it('should render a text input', () => { 

        let elementMarkUp = React.renderToStaticMarkup(<Input
                            key={ input.id }
                            type={ input.type }
                            optionValues={ input.value } 
                            labelName={ input.label } 
                            id={ input.id } 
                            styles={ input.styles } 
                            placeholder={ input.placeholder }
                            required={ input.required }
                            response={ input.response }
                            inputRow={ input.row }
                            numberOfLine={ input.numberOfLine }
                            errorMessage={ input.errorMessage }
                        />);
        let actual = elementMarkUp;
        let expected = '<div style="padding-top:10px;color:black;" class="col-sm-4"><div><label required="" for="1">First Name  <span style="color:red;">*</span></label><input class="form-control" required="" type="text" id="1" placeholder="First name"/></div></div>';

        expect(actual).toIncludeJSX(expected);
    });

    it('should render checkout page with button', () => { 

    });
});
