import React from 'react';
import ReactDOM from 'react-dom';
import Checkout from '../../app/views/checkout';
import CheckoutItem from '../../app/components/checkout/checkout-item';
import TestUtils from 'react-addons-test-utils';
import expect from 'expect';
import expectJSX from 'expect-jsx';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { Provider } from 'react-redux';
import reducers from '../../app/reducers/reducers';
import { STATE_NAME } from '../../app/constants/constants';
expect.extend(expectJSX);

describe('checkout page component', () => { 
    let store = applyMiddleware(thunk)(createStore)(reducers);
    let item = store.getState()[STATE_NAME.SESSION_MERCHANDIZE].sections[0].inputs[0];

    it('should render checkout page with button name "Register Next Attendee"', () => { 

        let element = TestUtils.renderIntoDocument(<Provider store={ store }><Checkout /></Provider>);
        let actual = ReactDOM.findDOMNode(element).textContent;
        let expected = 'Register Next Attendee';

        expect(actual).toIncludeJSX(expected);
    });

    it('should render checkout page with button', () => { 

        let element = TestUtils.renderIntoDocument(<Provider store={ store }><Checkout /></Provider>);
        let actual = TestUtils.scryRenderedDOMComponentsWithTag(element, 'button').length;
        let expected = 1;

        expect(actual).toEqual(expected);

    });

    it('should render CheckItem component', () => { 
        store.dispatch({ 
            type: 'ADD_TO_CART',
            item,
            quantity: 5
        });

        let element = TestUtils.renderIntoDocument(<Provider store={ store }><Checkout /></Provider>);
        let actual = TestUtils.findRenderedComponentWithType(element, CheckoutItem);
        
        expect(actual).toExist();
    });
});
