# Description

This is a simple react with redux demo project that I created to learn React and Redux.

This project uses react version 0.14.X, ECMASCRIPT 2015 (ES6), react-redux, react-router, react-bootstrap, and Webpack as the module bundler.

---
# How to set up and run the project

1. Clone this project: ```git@bitbucket.org:divinetouch/react-with-redux-demo-addon.git```
2. Go to the project folder/directory: ```cd react-with-redux-demo-addon```
3. Run npm install: ```npm install```
4. Start the server: ```npm start```
5. Open a browser and enter this url: ```localhost:8080```

- to build the project, simply execute the command : ```webpack``` (for production: ```webpacøk -p```)

# How to run test

1. go to the project folder/directory
2. To run test once, execute a command: ```npm run test```
3. To run test and watch for change, execute a command: ```npm run test-w```
