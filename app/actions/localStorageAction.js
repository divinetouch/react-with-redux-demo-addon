/**
 * This implementation allow the support for page refresh where we can read the last saved information for localStorage
 * and show it back to the user.
 *
 * It does not have to be a localStorage, it can be sessionStorage or an ajax call to the server to get the last saved 
 * state of the current session.
 */
import { updateAllApplicationState } from './applicationStateAction';
import { updateAllProfileResponse } from './profileAction';
import { updateAllSurveyResponse } from './surveyAction';
import { updateRegTypeResponse } from './regTypeAction';
import { updateSessionResponse } from './sessionAction';
import { updateCounterState } from './counterAction';
import { updateAllCartItems } from './cartAction';
import { updateRouteState } from './routeAction';
import { STATE_NAME } from '../constants/constants';

/**
 * Save the current state of the application to local storage
 */
export const writeToLocalStorage = (state) => { 

    Object.keys(state).forEach(key => { 
        //we don't want to save regTypes and Sessions to local storage as they can be much bigger than 5MB
        if(key !== STATE_NAME.REGTYPE_MERCHANDIZE && key !== STATE_NAME.SESSION_MERCHANDIZE) { 
            localStorage.setItem(key, JSON.stringify(state[key]));
        }
    });
    
    //log current size of state in MB if not in test mode
    if(!window.__TEST__) {
        console.group('state size');
        console.info(JSON.stringify(localStorage).length/100000 + ' MB');
        console.groupEnd('state size');
    }
};

/**
 * Read all the state value from the local storage
 */
export const readFromLocalStorage = () => { 
    return (dispatch, getState) => { 
        Object.keys(getState()).forEach(key => { 
            if(localStorage.getItem(key)) { 
                switch(key){ 
                    case STATE_NAME.APPLICATION_STATE:
                        dispatch(updateAllApplicationState(JSON.parse(localStorage.getItem(key))));
                        break;
                    case STATE_NAME.APPLICATION_ROUTE:
                        dispatch(updateRouteState(JSON.parse(localStorage.getItem(key))));
                        break;
                    case STATE_NAME.CART:
                        let cartItems = JSON.parse(localStorage.getItem(key)).cartItems;
                        //Use cart items to update purchased sessions and reg type
                        cartItems.forEach(item => { 
                            dispatch(updateRegTypeResponse(item.id, item.quantity));
                            dispatch(updateSessionResponse(item.id, item.quantity));
                        });
                        dispatch(updateAllCartItems(JSON.parse(localStorage.getItem(key))));
                        break;
                    case STATE_NAME.COUNTER:
                        dispatch(updateCounterState(JSON.parse(localStorage.getItem(key))));
                        break;
                    case STATE_NAME.PROFILE_INPUTS:
                        dispatch(updateAllProfileResponse(JSON.parse(localStorage.getItem(key))));
                        break;
                    case STATE_NAME.SURVEY_INPUTS:
                        dispatch(updateAllSurveyResponse(JSON.parse(localStorage.getItem(key))));
                        break;
                    default:
                        break;
                } 
            } 
        });
    };
};

/**
 * Get an object for localStorage with a specific key
 * @param key: a key string
 */
export const getObjectFromLocalStorageWithKey = (key) => { 
    return JSON.parse(localStorage.getItem(key));
};
