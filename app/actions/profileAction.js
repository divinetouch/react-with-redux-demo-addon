/**
 * This component handle all the actions related to profile
 *
 */
export const UPDATE_PROFILE = 'UPDATE_PROFILE';
export const TOGGLE_SECTION_VIEW = 'TOGGLE_SECTION_VIEW';
export const HIDE_SECTION_VIEW = 'HIDE_SECTION_VIEW';
export const SHOW_SECTION_VIEW = 'SHOW_SECTION_VIEW';
export const UPDATE_ALL_PROFILE_RESPONSE = 'UPDATE_ALL_PROFILE_RESPONSE';
export const RESET_PROFILE_RESPONSE = 'RESET_PROFILE_RESPONSE';

/**
 * When inputs (text, select, radio, checkbox, etc) in the profile page or sections changes/update this method will be called
 * to update the profile response state. The state then handles down the new input value and the inputs value got
 * updated accordingly
 *
 * @param id : an id of the input
 * @param response : the value or response of the input (this is how state and input value got synchronized)
 */
export const updateProfileResponse = (id, response) => ({ type: UPDATE_PROFILE, id, response });

/**
 * Profile inputs can be separated into different sections and we can hide or show each of the section
 * by calling this method
 * @param sectionName : the name of the section that we want to toggle
 */
export const toggleSectionView = (sectionName) => ({ type: TOGGLE_SECTION_VIEW, sectionName });

/**
 * Profile inputs can be separated into different sections and we can hide each of the section
 * by calling this method
 * @param sectionName : the name of the section that we want to hide
 */
export const hideSectionView = (sectionName) => ({ type: HIDE_SECTION_VIEW, sectionName });

/**
 * Profile inputs can be separated into different sections and we can show each of the section
 * by calling this method
 * @param sectionName : the name of the section that we want to show
 */
export const showSectionView = (sectionName) => ({ type: SHOW_SECTION_VIEW, sectionName });

/**
 * Sometime we want to update all the profile input object. For example, we want to update all the profile input objects that
 * has error to has a new property called error message and that error message will be shown under the input fields on the HTML
 * page itself.
 * @param newProfileResponseState : the new object and will be used to replace the old on in the current state
 */
export const updateAllProfileResponse = (newProfileResponseState) => ({ type: UPDATE_ALL_PROFILE_RESPONSE , newProfileResponseState});

/**
 * reset all profile inputs, for example, when we start registering a new attendee
 */
export const resetProfileResponse = () => ({ type: RESET_PROFILE_RESPONSE });
