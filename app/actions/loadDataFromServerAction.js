/**
 * During the initial load of the application we have to load the initial data/state from the server
 * Before the application is ready the application state have applicationIsReady value as false and after the data is
 * loaed then the state of the application is changed to true
 */
import { updateApplicationState } from './applicationStateAction';

//TODO: implementation
export const loadInitialDataFromServer = () => { 
    return (dispatch) => { 
        setTimeout(() => { 
            dispatch(updateApplicationState({ 
                isReady: true
            })); 
        },0);
    };
};
