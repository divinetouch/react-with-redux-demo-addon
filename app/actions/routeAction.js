import { pushPath, replacePath } from 'redux-simple-router';
import { validateRegistrationInputs } from '../validations/regTypeInputsValidator.js';
import { inputValidator } from '../validations/inputValidator';
import { updateApplicationState, saveAttendee } from './applicationStateAction';
import { ERROR } from '../constants/constants';
import { updateAllProfileResponse } from './profileAction';
import { updateAllSurveyResponse } from './surveyAction';
import { writeToLocalStorage, readFromLocalStorage } from './localStorageAction';
import { findInputAndSectionWithLabelName } from '../reducers/utils/search';

export const UPDATE_ACTIVE_ROUTE = 'UPDATE_ACTIVE_ROUTE';
export const UPDATE_ROUTE_STATE = 'UPDATE_ROUTE_STATE';
export const DISABLE_PREVIOUS_ROUTE = 'DISABLE_PREVIOUS_ROUTE';

/**
 * Update the current active route, for instance, profile, survey, registration etc to be the active one
 * @param newRoute
 */
export const updateRoute = (newRoute) => ({ type: UPDATE_ACTIVE_ROUTE, newRoute });

/**
 * Update the whole state. For instance, page refresh and has to read data from local storage
 * @param newState: a new application state that will replace the old one
 */
export const updateRouteState = (newState) => ({ type: UPDATE_ROUTE_STATE, newState });

export const disablePreviousRoutes = (newRoute) => ({ type: DISABLE_PREVIOUS_ROUTE, newRoute });

export const updateActiveRoute = (requestRoute) => {
    return (dispatch, getState) => {
        dispatch(readFromLocalStorage());
        let newRoute = requestRoute.replace(/\/|#/g, '').toLowerCase();
        let paths = getState().applicationRoute.paths.filter(path => {
            if(path.breadCrumb){
                return path;
            }
        });
        let currentActivePathIndex = paths.findIndex(path => path.active === true);
        let newPathIndex = paths.findIndex(path => path.path.toLowerCase() === newRoute);

        /**
         * Path is part of the breadcrumb
         */
        if(newPathIndex > -1) {
            /**
             * The default landing page
             * TODO: right now is registration page
             */
            if(currentActivePathIndex === -1) {
                dispatch(updateRoute(paths[0].path.toLowerCase()));
                dispatch(pushPath(paths[0].path.toLowerCase()));
            }
            /**
             * The path is authorized (part of the bread crumb)
             */
            else {
                dispatch(updateRoute(newRoute));
                /**
                * At checkout so disable all prvious route
                * and does not allow attendee or user to go any other route without registerting
                * a new attendee or complete the registration.
                */
                if(newPathIndex === paths.length-1) {
                    dispatch(disablePreviousRoutes(paths[newPathIndex].path));
                    dispatch(pushPath(newRoute));
                }
                /**
                 * attendee is already at chekcout page
                 */
                else if (currentActivePathIndex === paths.length-1) {
                    dispatch(replacePath(paths[currentActivePathIndex].path.toLowerCase()));
                }
                /**
                 * Just render what ever the view that the user is asking for
                 */
                else {
                    dispatch(pushPath(newRoute));
                }
            }
        }
        /**
         * Path is not part of the breadcrumb
         */
        else {
            currentActivePathIndex = getState().applicationRoute.paths.findIndex(path => {
                if(path.breadCrumb === undefined) {
                    return path.path.toLowerCase() === newRoute;
                }
            });
            /**
             * We do not update the active route when the route is not part of the breadcrumb
             * because we're only interested in controlling which route can a user/registrant
             * can go to when the route is part of the registration process, otherwise, no need
             * to care (with this current design).
             */
            if(currentActivePathIndex > -1) {
                dispatch(pushPath(newRoute));
            } else {
                dispatch(pushPath('404'));  // Page not found
            }
        }

        writeToLocalStorage(getState());
    };
};

/**
 * The action is called when we want to move to a new page
 * @param newRoute
 * @returns {Function}
 */
export const changeRoute = (newRoute) => {
    return (dispatch) => {
        dispatch(updateActiveRoute(newRoute));
        dispatch(pushPath(newRoute.toLowerCase()));
    };
};

/**
 * This action is called when want to move to a new page from the current active route
 * For example, the current active route is registration so the next route to move to is profile.
 * Remember this can all be dynamically configured and change
 * @returns {Function}
 */
export const moveToNext = () => {
    return (dispatch, getState) => {
        let paths = getState().applicationRoute.paths.filter(path => {
            if(path.breadCrumb){
                return path;
            }
        });
        let index = paths.findIndex(path => path.active === true);
        writeToLocalStorage(getState());
        /**
         * keep moving to next path and if the path is the last one
         * then circle back to the first one (this is the case where we register multiple
         * attendees. Also if path not found the index is -1, so the next path is 0 in the paths array
         */
        if(index < paths.length -1) {
            dispatch(updateActiveRoute(paths[index+1].path));
        } else {
            dispatch(updateActiveRoute(paths[0].path));
            dispatch(pushPath(paths[0].path.toLowerCase()));
        }
    };
};

/**
 * This action is called when want to move to the previous page from the current active route
 * For example, the current active route is profile so the next route to move to is registration.
 * Remember this can all be dynamically configured and change
 * @returns {Function}
 */
export const moveToPrevious = () => {
    return (dispatch, getState) => {
        let paths = getState().applicationRoute.paths;
        let index = paths.findIndex(path => path.active === true);
        writeToLocalStorage(getState());
        dispatch(updateActiveRoute(paths[index-1].path));
        dispatch(pushPath(paths[index-1].path.toLowerCase()));
    };
};


/**
 * This action is called when we want to make sure a reg type is selected before moving to the next page
 * @returns {Function}
 */
export const validateRegTypeAndMoveNext = () => {
    return (dispatch, getState) => {
        let sections = getState().regTypeMerchandize.sections;
        let currentApplicationState = getState().applicationState;
        if(validateRegistrationInputs(sections)) {
            dispatch(moveToNext());
        } else {
            dispatch(updateApplicationState(
                {
                    hasError: true,
                    errorMessage: ERROR.REGTYPE_ERROR,
                    regType: currentApplicationState.currentRegType,
                    regCategory: currentApplicationState.currentRegCategory
                })
            );
        }
    };
};

/**
 * No Error Found so update the state and move to next route
 * @param currentApplicationState
 */
let noErrorMoveNext = (dispatch, currentApplicationState, firstName, lastName) => {
    dispatch(updateApplicationState(
        {
            hasError: false,
            firstName: firstName ? firstName: currentApplicationState.firstName,
            lastName: lastName ? lastName: currentApplicationState.lastName,
            index: undefined
        })
    );
    dispatch(moveToNext());
};

/**
 * The first name and last name input fields can be anywhere as part of the profile inputs so
 * This method is for finding and getting the value of the first name and last name
 * @param profileInputs: is the profileInputs object (state object)
 */
let getFirstAndLastName = (profileInputs) => {
    let { sectionIndex:sectionFirstNameIndex,inputIndex:inputFirstNameIndex } = findInputAndSectionWithLabelName(profileInputs, 'first name');
    let { sectionIndex:sectionLastNameIndex,inputIndex:inputLastNameIndex } = findInputAndSectionWithLabelName(profileInputs, 'last name');
    let firstName = profileInputs.sections[sectionFirstNameIndex].inputs[inputFirstNameIndex].response;
    let lastName = profileInputs.sections[sectionLastNameIndex].inputs[inputLastNameIndex].response;

    return {firstName:firstName, lastName:lastName};
};

/**
 * This action is called when we want to validate all profile inputs before moving to the new page
 * @returns {Function}
 */
export const validateProfileInputsAndMoveNext = () => {
    return (dispatch, getState) => {
        let {hasError:hasError, newResponseState: newProfileResponseState} = inputValidator.validateProfileInputs(getState().profileInputs);
        if(!hasError) {
            let {firstName:firstName, lastName:lastName}  = getFirstAndLastName(getState().profileInputs);
            noErrorMoveNext(dispatch, getState().applicationState, firstName, lastName);
        } else {
            dispatch(updateAllProfileResponse(newProfileResponseState));
            dispatch(updateApplicationState(
                {
                    hasError: true,
                    errorMessage: ERROR.GENERIC_ERROR
                })
            );
        }
    };
};

/**
 * This action is called when we want to validate all survey inputs before moving to the new page
 * @returns {Function}
 */
export const validateSurveyInputsAndMoveNext = () => {
    return (dispatch, getState) => {
        let {hasError:hasError, newResponseState: newSurveyResponseState} = inputValidator.validateSurveyInputs(getState().surveyInputs);
        let currentApplicationState = getState().applicationState;
        if(!hasError) {
            noErrorMoveNext(dispatch, currentApplicationState);
        } else {
            dispatch(updateAllSurveyResponse(newSurveyResponseState));
            dispatch(updateApplicationState(
                {
                    hasError: true,
                    errorMessage: ERROR.GENERIC_ERROR
                })
            );
        }
    };
};

/**
 * This action is called when we want to validate all inputs include both profile inputs and surveys input before moving to the new page
 * @returns {Function}
 */
export const validateAllInputsAndMoveNext = () => {
    return (dispatch, getState) => {
        let {hasError:hasProfileError, newResponseState: newProfileResponseState} = inputValidator.validateProfileInputs(getState().profileInputs);
        let {hasError:hasSurveyError, newResponseState: newSurveyResponseState} = inputValidator.validateSurveyInputs(getState().surveyInputs);
        let currentApplicationState = getState().applicationState;
        if(hasProfileError || hasSurveyError) {
            dispatch(updateApplicationState(
                {
                    hasError: true,
                    errorMessage: ERROR.GENERIC_ERROR,
                    currentRegType: currentApplicationState.currentRegType,
                    currentRegCategory: currentApplicationState.currentRegCategory
                })
            );
            dispatch(updateAllSurveyResponse(newSurveyResponseState));
            dispatch(updateAllProfileResponse(newProfileResponseState));
        } else {
            dispatch(saveAttendee());
            noErrorMoveNext(dispatch, getState().applicationState);
        }
    };
};
