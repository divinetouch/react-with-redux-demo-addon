import { addToCart, removeFromCart } from './cartAction';

export const UPDATE_SESSION = 'UPDATE_SESSION';
export const UPDATE_SESSION_STATE = 'UPDATE_SESSION_STATE';
export const RESET_SESSION = 'RESET_SESSION';

/**
 * This action is used to dispatch to update the session state
 * @param id
 * @param response
 */
export const updateSessionResponse = (id, response) => ({ type: UPDATE_SESSION, id, response });

/**
 * Update the whole state. For instance, page refresh and has to read data from local storage
 * @param newState: a new application state that will replace the old one
 */
export const updateSessionState = (newState) => ({ type: UPDATE_SESSION_STATE, newState });

export const resetSession = () => ({ type: RESET_SESSION });

/**
 * This action is called when a session item is being add to or remove from the cart
 * @param session : session item or object
 * @param quantity : the quantity
 * @returns {Function}
 */
export const updateSession = (session, quantity) => { 
    return (dispatch) => {
        /**
         * If the quantity is greater then zero then whe add
         * Otherwise, we remove
         */
        if(quantity > 0) {
            dispatch(updateSessionResponse(session.id, quantity));
            dispatch(addToCart(session, quantity));
        } else {
            dispatch(removeFromCart(session, quantity));
            dispatch(updateSessionResponse(session.id));
        }
    };
};
