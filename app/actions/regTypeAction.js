import { updateApplicationState } from './applicationStateAction';
import { addToCart, removeFromCart } from './cartAction';
export const UPDATE_REGTYPE = 'UPDATE_REGTYPE';
export const UPDATE_REGTYPE_STATE = 'UPDATE_REGTYPE_STATE';
export const RESET_REGTYPE = 'RESET_REGTYPE';

/**
 * This action will update the response/quantity in the reg type object
 * @param id
 * @param response
 */
export const updateRegTypeResponse = (id, response) => ({ type: UPDATE_REGTYPE, id, response });

/**
 * Reset all reg type object to default value
 */
export const updateRegTypeState = (newState) => ({ type: UPDATE_REGTYPE_STATE, newState });

/**
 * Reset all reg type object to default value
 */
export const resetRegType = () => ({ type: RESET_REGTYPE});

/**
 * This action is called when a reg type is selected or deselected. We have to dispatch a few actions like update the current reg
 * type, all the reg type to the shopping cart and update application state with the new reg type. We have to do the opposite
 * thing when a reg type got deselected.
 *
 * @param regType : a reg type object
 * @param quantity : The amount (default to 1 or 0 according to the business rule)
 */
export const updateRegType = (regType, quantity) => {
    return (dispatch, getState) => {
        /**
         * If the quantity is greater than zero then we want to add
         * Otherwise, we want to remove
         */
        if(quantity > 0) {
            let currentRegType = getState().applicationState.currentRegType;
            if(currentRegType && regType.id !== currentRegType.id) {
                dispatch(removeFromCart(getState().applicationState.currentRegType));
            }
            dispatch(updateRegTypeResponse(regType.id, quantity));
            dispatch(updateApplicationState({
                hasError: false,
                regType: regType
            }));
            dispatch(addToCart(regType, quantity));
        } else {
            dispatch(removeFromCart(regType));
            dispatch(updateRegTypeResponse(regType.id));
        }
    };
};
