/**
 * This action component is purely related to handling any actions that related to
 * shopping cart. Sometime when we remove an item from a cart we also have to dispatch other
 * actions that are not handle by this component, for example, removeFromShoppingCart action has
 * also dispatch updateRegType and updateSessionResponse actions
 */

import { updateRegType } from './regTypeAction';
import { updateSessionResponse } from './sessionAction';

export const ADD_TO_CART = 'ADD_TO_CART';
export const REMOVE_FROM_CART = 'REMOVE_FROM_CART';
export const REMOVE_OTHER_FROM_CART = 'REMOVE_OTHER_FROM_CART';
export const UPDATE_ALL_CART_ITEMS = 'UPDATE_ALL_CART_ITEMS';
export const RESET_CART = 'RESET_CART';

/**
 * an action that can be dispatched to remove an item to a cart
 * This function won't be called by the other components but is called by other actions
 * @param item
 */
export const removeFromCart = (item) => ({ type: REMOVE_FROM_CART, item });

/**
 * an action that can be dispatched to add an item to a cart
 * This function won't be called by the other components but is called by other actions
 * @param item
 */
export const addToCart = (item, quantity) => ({ type: ADD_TO_CART, item, quantity });

/**
 * Update the whole state. For instance, page refresh and has to read data from local storage
 * @param newState: a newState that replace the old state
 */
export const updateAllCartItems = (newCart) => ({ type: UPDATE_ALL_CART_ITEMS, newCart });

/**
 * The action can be called when we want to remove an item from a cart
 * If the item is happened to be  a regType then the profile reg type is also going to be updated
 * If the item is a session then it is going to be just removed like any other item
 * @param item
 * @returns {Function}
 */
export const removeFromShoppingCart = (item) => { 
    return (dispatch) => { 
        dispatch(removeFromCart(item)); 
        dispatch(updateRegType(item));
        dispatch(updateSessionResponse(item.id));
    };
};

/**
 * reset cart to empty, for example, when we saved the current attendee and start registering a new attendee
 */
export const resetCart = () => ({ type: RESET_CART });
