//counter
export const DECREMENT = 'DECREMENT';
export const INCREMENT = 'INCREMENT';
export const UPDATE_COUNTER_STATE = 'UPDATE_COUNTER_STATE';

//counter
export const incrementCounter = () => ({ type: INCREMENT });
export const decrementCounter = () => ({ type: DECREMENT });


/**
 * Update the whole state. For instance, page refresh and has to read data from local storage
 * @param newState: a new application state that will replace the old one
 */
export const updateCounterState = (newState) => ({ type: UPDATE_COUNTER_STATE, newState });
