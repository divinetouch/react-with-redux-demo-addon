//survey
export const UPDATE_SURVEY = 'UPDATE_SURVEY';
export const UPDATE_ALL_SURVEY_RESPONSE = 'UPDATE_ALL_SURVEY_RESPONSE';
export const RESET_SURVEY_RESPONSE = 'RESET_SURVEY_RESPONSE';

//survey
export const updateSurveyResponse = (id, response) => ({ type: UPDATE_SURVEY, id, response });

/**
 * Update the whole state. For instance, page refresh and has to read data from local storage
 * @param newState: a new application state that will replace the old one
 */
export const updateAllSurveyResponse = (newSurveyResponseState) => ({ type: UPDATE_ALL_SURVEY_RESPONSE, newSurveyResponseState });

/**
 * reset all surveys inputs, for example, we we save the current attendee and start registering a new one
 */
export const resetSurveyResponse = () => ({ type: RESET_SURVEY_RESPONSE });
