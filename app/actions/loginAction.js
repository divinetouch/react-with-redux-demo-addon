/**
 * Authenticate user require communicating with server
 */
import { updateApplicationState } from './applicationStateAction';
import { moveToNext } from './routeAction';

//TODO: implementation
export const login = (credentials) => { 
    return (dispatch) => { 
        setTimeout(() => { 
            dispatch(updateApplicationState({ 
                isReady: true
            })); 
            dispatch(moveToNext());
        },2000);
    };
};
