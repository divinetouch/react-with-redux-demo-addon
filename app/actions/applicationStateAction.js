/**
 * Maintaining the global state of the application/profile
 *
 * What does that mean? 
 *
 * That means, error free state or have error state, supporting multiple profiles and guests
 * Allow switching between different profiles/attendee, making changes, and saving changes.
 * If an error occur, we can state the application state to having error and with what ever error message we want, then
 * the page and component can decide what to do with the error message.
 */

import { resetProfileResponse, updateAllProfileResponse } from './profileAction';
import { resetSurveyResponse, updateAllSurveyResponse } from './surveyAction';
import { resetCart, updateAllCartItems } from './cartAction';
import { resetSession, updateSessionResponse } from './sessionAction';
import { resetRegType, updateRegTypeResponse } from './regTypeAction';
import { writeToLocalStorage, getObjectFromLocalStorageWithKey } from './localStorageAction';
import { STATE_NAME } from '../constants/constants';

export const UPDATE_APPLICATION_STATE = 'UPDATE_APPLICATION_STATE';
export const UPDATE_ALL_APPLICATION_STATE = 'UPDATE_ALL_APPLICATION_STATE';
export const SAVE_NEW_ATTENDEE = 'SAVE_NEW_ATTENDEE';
export const UPDATE_ATTENDEE = 'UPDATE_ATTENDEE';

/**
 * update the global state of the application/profile
 * @param args: is an object with keys: hasError, errorMessage, regType, regCategory, index, isAdmin.
 */
export const updateApplicationState = (args) => ({ type: UPDATE_APPLICATION_STATE, args});

/**
 * Update the whole state. For instance, page refresh and has to read data from local storage
 * @param newState: a new application state that will replace the old one
 */
export const updateAllApplicationState = (newState) => ({ type: UPDATE_ALL_APPLICATION_STATE, newState });

/**
 * When we want to save the current attendee and start registering a new attendee
 * @param profile: this is the profileInputs state object
 * @param survey: this is the surveyInputs state object
 * @param cart: this is the cart state object
 */
export const saveNewAttendee = (profileInputs, surveyInputs, cart) => ({ type: SAVE_NEW_ATTENDEE, profileInputs, surveyInputs, cart });

/**
 * When we want to update an attendee and start the process of modifying this specific attendee details 
 * @param index: the index of the attendee being modify (this index is set in the application State object
 * @param profile: this is the profileInputs state object
 * @param survey: this is the surveyInputs state object
 * @param cart: this is the cart state object
 */
export const updateAttendee = (index, profileInputs, surveyInputs, cart) => ({ type: UPDATE_ATTENDEE, index, profileInputs, surveyInputs, cart });

/**
 * This action can be called to save the attendee to the application state
 * After the all the attendee information is saved then the current profileInputs, surveyInputs, and cart state will be
 * reset so we can start registering a new attendee.
 */
export const saveAttendee = () => { 
    return (dispatch, getState) => { 
        if(getState().applicationState.index !== undefined) { 
            dispatch(updateAttendee(
                        getState().applicationState.index,
                        getObjectFromLocalStorageWithKey(STATE_NAME.PROFILE_INPUTS),
                        getObjectFromLocalStorageWithKey(STATE_NAME.SURVEY_INPUTS),
                        Object.assign({}, {...getState().cart})
                    ));
        } else {
            dispatch(saveNewAttendee(
                        getObjectFromLocalStorageWithKey(STATE_NAME.PROFILE_INPUTS),
                        getObjectFromLocalStorageWithKey(STATE_NAME.SURVEY_INPUTS),
                        Object.assign({}, {...getState().cart})
                    ));
        }
        dispatch(resetProfileResponse());
        dispatch(resetSurveyResponse());
        dispatch(resetCart());
        dispatch(resetRegType());
        dispatch(resetSession());
        writeToLocalStorage(getState());
    };
};

/**
 * This action is called/dispatched when we want to modify one of the already saved attendee
 * In order to allow the application to know which attendee is being modify we have to set the index in the application
 * state which is the index of the saved attendee in the profiles array. So when it's time to save the update the
 * application knows which profile in the array has to replaced/updated with the new information.
 */
export const switchToProfile = (profile) => { 
    return (dispatch, getState) => { 
        dispatch(updateApplicationState({index: profile.index, regType: profile.regType, regCategory: profile.regCategory}));
        profile.cart.cartItems.forEach(item => { 
            dispatch(updateRegTypeResponse(item.id, item.quantity));
            dispatch(updateSessionResponse(item.id, item.quantity));
        });
        dispatch(updateAllProfileResponse(profile.profileInputs));
        dispatch(updateAllSurveyResponse(profile.surveyInputs));
        dispatch(updateAllCartItems(profile.cart));
        writeToLocalStorage(getState());
    };
};
