import { UPDATE_ACTIVE_ROUTE, UPDATE_ROUTE_STATE, DISABLE_PREVIOUS_ROUTE } from '../actions/routeAction';
import { testData } from '../testData/routeTestData';

let initialState = testData;

/**
 * If we decide to use object instead of array
 */
// let resetActiveRoute = (state, activeRoute) => { 
//     Object.keys(state).forEach(key => { 
//         key === activeRoute ? state[key].active = true: state[key].active = false;
//     });
//     return {...state};
// };
//
let resetActiveRoute = (state, index) => { 
    state.paths.forEach((path, currentIndex) => {
        if(currentIndex > index && path.breadCrumb) {
            path.disabled = true; 
        }
        path.active = false;
    });
    return state;
};

let disableAllPreviousRoute = (state, index) => { 
    state.paths.forEach((path, currentIndex) => {
        if(currentIndex < index && path.breadCrumb) {
            path.disabled = true; 
        }
        path.active = false;
    });
    return state;
};

export default(state=initialState, action) => { 

    switch(action.type) {
        case UPDATE_ACTIVE_ROUTE:
            let index = state.paths.findIndex(path => path.path.toLowerCase() === action.newRoute.toLowerCase());
            let resetState = resetActiveRoute(state, index);
            if (index !== undefined) {
                return Object.assign({}, 
                    ...resetState,
                    {
                        paths: [
                            ...resetState.paths.slice(0, index),
                            Object.assign({}, { 
                                ...resetState.paths[index],
                                active: true,
                                disabled: false
                            }),
                            ...resetState.paths.slice(index+1)
                        ]
                    });
            }
            return state;
        case UPDATE_ROUTE_STATE:
            return Object.assign({}, action.newState);
        case DISABLE_PREVIOUS_ROUTE:
            index = state.paths.findIndex(path => path.path.toLowerCase() === action.newRoute.toLowerCase());
            resetState = disableAllPreviousRoute(state, index);
            if (index !== undefined) {
                return Object.assign({}, 
                    ...resetState,
                    {
                        paths: [
                            ...resetState.paths.slice(0, index),
                            Object.assign({}, { 
                                ...resetState.paths[index],
                                active: true,
                                disabled: false
                            }),
                            ...resetState.paths.slice(index+1)
                        ]
                    });
            }
            return state;
        default:
            return state;
    }

};
