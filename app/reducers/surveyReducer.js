import { RESET_SURVEY_RESPONSE, UPDATE_SURVEY, UPDATE_ALL_SURVEY_RESPONSE } from '../actions/surveyAction';
import { testData } from '../testData/surveyTestData';

let initialState = testData;

let resetAllInputs = (state) => { 
    state.inputs.forEach((input, inputIndex) => { 
        input.response = undefined;
        input.errorMessage = undefined;
    });
    return Object.assign({}, {...state});
};

export default (state=initialState, action) => { 
    switch (action.type) {
        case UPDATE_SURVEY:
            let index = state.inputs.findIndex(input => input.id === action.id);
            return Object.assign({},{ inputs: [
                ...state.inputs.slice(0, index),
                Object.assign({}, {...state.inputs[index], errorMessage: undefined, response: action.response}),
                ...state.inputs.slice(index+1)
            ]});
        case UPDATE_ALL_SURVEY_RESPONSE:
            return Object.assign({}, action.newSurveyResponseState);
        case RESET_SURVEY_RESPONSE: 
            return resetAllInputs(state);
        default:
            return state;
    } 
};
