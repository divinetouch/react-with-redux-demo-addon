import { RESET_CART, ADD_TO_CART, REMOVE_FROM_CART, UPDATE_ALL_CART_ITEMS } from '../actions/cartAction';
import CartAPI from '../cart-api/cartAPI';

let initialState = {
    cartItems: []
};

export default(state=initialState, action) => { 
    switch (action.type) {
        case ADD_TO_CART:
            return Object.assign({},
                {
                    ...state,
                    cartItems: CartAPI.addToCart(state.cartItems, action.item, action.quantity)
                }
            );
        case REMOVE_FROM_CART:
            return Object.assign({}, { ...state, cartItems: CartAPI.removeFromCart(state.cartItems, action.item)});
        case RESET_CART:
            return Object.assign({}, {cartItems:[]});
        case UPDATE_ALL_CART_ITEMS:
            return Object.assign({}, action.newCart);
        default:
            return state;
    }
};
