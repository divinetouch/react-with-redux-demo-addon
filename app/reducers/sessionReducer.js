import { RESET_SESSION,  UPDATE_SESSION, UPDATE_SESSION_STATE } from '../actions/sessionAction';
import { testData } from '../testData/sesssionTestData';
import { findInputAndSectionIndex, resetAllInputs } from './utils/search';

let initialState = testData;

export default (state=initialState, action) => { 
    switch(action.type) { 
        case UPDATE_SESSION:
            let { sectionIndex:sectionInd, inputIndex:inputInd } = findInputAndSectionIndex(state, action.id);
            if(inputInd !== undefined) {
                return Object.assign({}, { sections: [
                    ...state.sections.slice(0, sectionInd),
                    Object.assign({},{
                        ...state.sections[sectionInd],
                        inputs: [
                            ...state.sections[sectionInd].inputs.slice(0, inputInd),
                            Object.assign({}, {...state.sections[sectionInd].inputs[inputInd], response: action.response === undefined ? 0: action.response}),
                            ...state.sections[sectionInd].inputs.slice(inputInd+1)
                        ] 
                    }),
                    ...state.sections.slice(sectionInd+1)
                ]});
            }
            return state;
        case UPDATE_SESSION_STATE:
            return Object.assign({}, action.newState);
        case RESET_SESSION:
            return resetAllInputs(state);
        default:
            return state;
    } 
};
