import { UPDATE_PROFILE,TOGGLE_SECTION_VIEW,HIDE_SECTION_VIEW,SHOW_SECTION_VIEW, UPDATE_ALL_PROFILE_RESPONSE, RESET_PROFILE_RESPONSE } from '../actions/profileAction';
import { testData } from '../testData/profileTestData';
import { resetAllInputs, findInputAndSectionIndex, findSectionIndex } from './utils/search';

let initialState = testData;

export default (state=initialState, action) => { 
    switch (action.type) {
        case UPDATE_PROFILE:
            let { sectionIndex:sectionInd, inputIndex:inputInd } = findInputAndSectionIndex(state, action.id);

            return Object.assign({}, { sections: [
                ...state.sections.slice(0, sectionInd),
                Object.assign({},{
                    ...state.sections[sectionInd],
                    inputs: [
                        ...state.sections[sectionInd].inputs.slice(0, inputInd),
                        Object.assign({}, {...state.sections[sectionInd].inputs[inputInd], errorMessage: undefined, response: action.response}),
                        ...state.sections[sectionInd].inputs.slice(inputInd+1)
                    ] 
                }),
                ...state.sections.slice(sectionInd+1)
            ]});
        case TOGGLE_SECTION_VIEW:
            let sectionIndex = findSectionIndex(state, action.sectionName);
            return Object.assign({}, { sections: [
                ...state.sections.slice(0, sectionIndex),
                Object.assign({},{
                    ...state.sections[sectionIndex],
                    show: !state.sections[sectionIndex].show
                }),
                ...state.sections.slice(sectionIndex+1)
            ]});
        case HIDE_SECTION_VIEW:
            sectionIndex = findSectionIndex(state, action.sectionName);
            return Object.assign({}, { sections: [
                ...state.sections.slice(0, sectionIndex),
                Object.assign({},{
                    ...state.sections[sectionIndex],
                    show: false
                }),
                ...state.sections.slice(sectionIndex+1)
            ]});
        case SHOW_SECTION_VIEW:
            sectionIndex = findSectionIndex(state, action.sectionName);
            return Object.assign({}, { sections: [
                ...state.sections.slice(0, sectionIndex),
                Object.assign({},{
                    ...state.sections[sectionIndex],
                    show: true
                }),
                ...state.sections.slice(sectionIndex+1)
            ]});
        case UPDATE_ALL_PROFILE_RESPONSE:
            return Object.assign({}, action.newProfileResponseState);
        case RESET_PROFILE_RESPONSE:
            return resetAllInputs(state);
        default:
            return state;
    } 
};
