/**
 * reduce can be split to multiple reducer for example cartItem, profile, purchase history, etc.
 * To test this I created a simple counter that will be combined with merchansize reducer.
 */
import counterReducer from './counterReducer';
import profileReducer from './profileReducer';
import surveyReducer from './surveyReducer';
import sessionReducer from './sessionReducer';
import regTypeReducer from './regTypeReducer';
import cartReducer from './cartReducer';
import applicationRouteReducer from './routeReducer';
import { routeReducer } from 'redux-simple-router';
import applicationStateReducer from './applicationStateReducer';
import { combineReducers } from 'redux';
import { STATE_NAME } from '../constants/constants';

/**
 * When we first land on the home page, this is the initial state
 */

export default combineReducers({ 
    [STATE_NAME.CART]: cartReducer,
    [STATE_NAME.COUNTER]: counterReducer,
    [STATE_NAME.PROFILE_INPUTS]: profileReducer,
    [STATE_NAME.SURVEY_INPUTS]: surveyReducer,
    [STATE_NAME.SESSION_MERCHANDIZE]: sessionReducer,
    [STATE_NAME.REGTYPE_MERCHANDIZE]: regTypeReducer,
    [STATE_NAME.APPLICATION_STATE]: applicationStateReducer,
    [STATE_NAME.APPLICATION_ROUTE]: applicationRouteReducer,
    routing: routeReducer
});
