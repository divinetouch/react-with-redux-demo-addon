
/**
 * //TODO
 * properly not a good way to find id
 * splitting the profile itselt to different file depending on the section?
 * @param state: the redux/application state
 * @param inputId: the ID of the input
 */
export const findInputAndSectionIndex = (state, inputId) => { 
    let result = {};
    state.sections.forEach((section, sectionIndex) => { 
        section.inputs.forEach((input, inputIndex) => { 
            if(input.id === inputId) { 
                result = { sectionIndex: sectionIndex, inputIndex: inputIndex };
            }
        });
    });
    return result;
};

export const findInputAndSectionWithLabelName = (state, inputLabel) => { 
    let result = {};
    state.sections.forEach((section, sectionIndex) => { 
        section.inputs.forEach((input, inputIndex) => { 
            if(input.label.toLowerCase() === inputLabel.toLowerCase()) { 
                result = { sectionIndex: sectionIndex, inputIndex: inputIndex };
            }
        });
    });
    return result;
};

/**
 * inputs could be separated into multiple sections and given section name we can search for the index
 * of the section
 * @param state: the redux/application state
 * @param sectionName: name of the section
 */
export const findSectionIndex = (state, sectionName) => {
    return state.sections.findIndex((section) => section.sectionName.toUpperCase() === sectionName.toUpperCase());
};

/**
 * this is for searching for the regType that already selected and unselecting it to 
 * allow only select one regType.
 * @param state: the redux/application state
 */
export const findInputAndSectionWithQauntity1 = (state) => { 
    let result = {};
    state.sections.forEach((section, sectionIndex) => { 
        section.inputs.forEach((input, inputIndex) => { 
            if(input.response !== undefined) { 
                result = { sectionIndex: sectionIndex, inputIndex: inputIndex };
            }
        });
    });
    return result;
};

/**
 * reset all inputs to undefined
 */
export const resetAllInputs = (state) => { 
    state.sections.forEach((section, sectionIndex) => { 
        section.inputs.forEach((input, inputIndex) => { 
            input.response = undefined;
            input.errorMessage = undefined;
        });
    });
    return Object.assign({}, {...state});
};
