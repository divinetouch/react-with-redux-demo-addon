import * as ALL from '../actions/applicationStateAction';
import { testData } from '../testData/applicationStateTestData';

let initialState = testData;

export default(state=initialState, action) => {
    switch (action.type) {
        case ALL.UPDATE_APPLICATION_STATE:
            return Object.assign({}, { ...state,
                hasError: action.hasError,
                errorMessage: action.args.errorMessage,
                currentRegType: action.args.regType ? action.args.regType : state.currentRegType,
                previousRegType: state.currentRegType,
                currentRegCategory: action.args.regCategory ? action.args.regCategory : state.currentRegCategory,
                previousRegCategory: state.currentRegCategory,
                firstName: action.args.firstName ? action.args.firstName : state.firstName,
                lastName: action.args.lastName ? action.args.lastName: state.lastName,
                applicationIsReady: action.args.isReady ? action.args.isReady : state.applicationIsReady,
                index: action.args.index !== undefined ? action.args.index : state.index
            });
        case ALL.UPDATE_ALL_APPLICATION_STATE:
            return Object.assign({}, action.newState);
        case ALL.SAVE_NEW_ATTENDEE:
            return Object.assign({}, {
                ...state,
                firstName: undefined,
                lastName: undefined,
                currentRegType: undefined,
                previousRegType: undefined,
                currentRegCategory: undefined,
                previousRegCategory: undefined,
                hasError: undefined,
                errorMessage: undefined,
                applicationIsReady: state.applicationIsReady,
                index: undefined,
                profiles: [
                    ...state.profiles,
                    Object.assign({}, {
                        profileInputs: action.profileInputs,
                        surveyInputs: action.surveyInputs,
                        cart: action.cart,
                        firstName: state.firstName,
                        lastName: state.lastName,
                        regType: state.currentRegType,
                        regCategory: state.currentRegCategory,
                        index: state.profiles.length
                    })
                ]
            });
        case ALL.UPDATE_ATTENDEE:
            return Object.assign({}, {
                ...state,
                firstName: undefined,
                lastName: undefined,
                currentRegType: undefined,
                previousRegType: undefined,
                currentRegCategory: undefined,
                previousRegCategory: undefined,
                hasError: undefined,
                errorMessage: undefined,
                applicationIsReady: state.applicationIsReady,
                index: undefined,
                profiles:  [
                    ...state.profiles.slice(0, action.index),
                    Object.assign({}, {
                        profileInputs: action.profileInputs,
                        surveyInputs: action.surveyInputs,
                        cart: action.cart,
                        firstName: state.firstName,
                        lastName: state.lastName,
                        regType: state.currentRegType,
                        regCategory: state.currentRegCategory,
                        index: action.index
                    }),
                    ...state.profiles.slice(action.index+1)
                ]
            });
        default:
            return state;
    }
};
