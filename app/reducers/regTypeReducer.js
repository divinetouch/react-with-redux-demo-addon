
import { RESET_REGTYPE, UPDATE_REGTYPE, UPDATE_REGTYPE_STATE } from '../actions/regTypeAction';
import { testData } from '../testData/regTypeTestData';
import { resetAllInputs, findInputAndSectionIndex, findInputAndSectionWithQauntity1 } from './utils/search';

let initialState = testData;

let resetRegType = (state) => { 
    let { sectionIndex:sectionInd1, inputIndex:inputInd1 } = findInputAndSectionWithQauntity1(state);
    if(inputInd1 !== undefined) {
        return Object.assign({}, {...state, sections: [
            ...state.sections.slice(0, sectionInd1),
            Object.assign({},{
                ...state.sections[sectionInd1],
                inputs: [
                    ...state.sections[sectionInd1].inputs.slice(0, inputInd1),
                    Object.assign({}, {
                        ...state.sections[sectionInd1].inputs[inputInd1], 
                        errorMessage: undefined,
                        response: undefined}),
                    ...state.sections[sectionInd1].inputs.slice(inputInd1+1)
                ] 
            }),
            ...state.sections.slice(sectionInd1+1)
        ]});
    }
    return state;
};

export default (state=initialState, action) => { 
    switch(action.type) { 
        case UPDATE_REGTYPE:
            let resetState = resetRegType(state);
            let { sectionIndex:sectionInd, inputIndex:inputInd } = findInputAndSectionIndex(resetState, action.id);
            if(inputInd !== undefined) {
                return Object.assign({},{
                    ...resetState,
                    sections: [
                        ...resetState.sections.slice(0, sectionInd),
                        Object.assign({},{
                            ...resetState.sections[sectionInd],
                            inputs: [
                                ...resetState.sections[sectionInd].inputs.slice(0, inputInd),
                                Object.assign({}, {...resetState.sections[sectionInd].inputs[inputInd], response: action.response}),
                                ...resetState.sections[sectionInd].inputs.slice(inputInd+1)
                            ] 
                        }),
                        ...resetState.sections.slice(sectionInd+1)
                    ]});
            }
            return state;
        case UPDATE_REGTYPE_STATE:
            return Object.assign({}, action.newState);
        case RESET_REGTYPE:
            return Object.assign({}, {...resetRegType(state)});
        default:
            return state;
    } 
};
