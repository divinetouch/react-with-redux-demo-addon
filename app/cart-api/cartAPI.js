/**
 * Cart API does not care what kind of item you put in the cart as long
 * as the added item has ID, Quanity, and Price.
 */
const CartAPI = { 

    findItemInCart(cartItems, item) { 
        return cartItems.findIndex(cartItem => cartItem.id === item.id);
    },

    addToCart(cartItems, item, quantity) { 
        let index = this.findItemInCart(cartItems, item);
        if(index === -1) { 
            return [...cartItems, {...item, quantity: quantity} ];
        } else { 
            return [...cartItems.slice(0, index),
                Object.assign({}, {...cartItems[index], quantity: quantity }),
                ...cartItems.slice(index+1)];
        } 
    },

    removeFromCart(cartItems, item) { 
        let index = this.findItemInCart(cartItems, item);
        if(index > -1) {
            return [...cartItems.slice(0, index), ...cartItems.slice(index+1)];
        } else { 
            return cartItems;
        }
    },

    getTotal(cartItems, total=0) { 
        cartItems.forEach(item => { 
            total += item.quantity * item.price; 
        });

        return total;
    }
};

export default CartAPI;
