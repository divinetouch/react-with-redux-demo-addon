import { ERROR } from '../constants/constants';

export const inputValidator = { 

    hasError: false,
   
    validateInput (input) { 
        if(input.required) { 
            if(!input.response) { 
                this.hasError = true;
                return Object.assign({}, { ...input, errorMessage: ERROR.REQUIRED_ERROR });
            } else { 
                return Object.assign({}, { ...input, errorMessage: undefined });
            }
        } else { 
            return input;
        }   
    },

    validateInputSection (inputs) { 
        inputs.forEach((input,index) => { 
            inputs[index] = this.validateInput(input);
        });
        return [...inputs];
    },

    validateSections (sections) { 
        sections.forEach((section, index) => { 
            if(section.show){
                sections = [
                    ...sections.slice(0,index),
                    Object.assign({},{...sections[index], inputs: this.validateInputSection(sections[index].inputs)}),
                    ...sections.slice(index+1)
                ];
            }
        });
        return [...sections];
    },

    validateProfileInputs(toValidate) { 
        this.hasError = false;
        return { 
            newResponseState: Object.assign({}, {...toValidate, sections: this.validateSections(toValidate.sections)}),
            hasError: this.hasError
        };
    },

    validateSurveyInputs(toValidate) { 
        this.hasError = false;
        return { 
            newResponseState: Object.assign({}, {...toValidate, inputs: this.validateInputSection(toValidate.inputs)}),
            hasError: this.hasError
        };
    }
};
