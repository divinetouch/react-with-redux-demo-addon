export const validateRegistrationInputs = (sections) => { 
    let result = undefined;
    sections.forEach(section => { 
        section.inputs.forEach(input => { 
            if(input.response !== undefined) { 
                result = input;
            }
        });
    });
    return result;
};
