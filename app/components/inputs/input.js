/**
 * the input is configure in such a way that every single value is hand down from the props
 * the props itself can be handed down from the state. So when an input change, it will trigger an action
 * the action then dispatch to to update the state, then the stat hand down all the new props that was changed
 * This current design is I believe a unidirectional data flow.
 *
 * WE ARE NOT USING COMPONENT STATE BUT THE TOP STATE TREE IS ALWAYS THE ONE THAT PROVIDE THE NEW STATE VALUE
 *
 * The handle of input change is now the resposibility of the parent, the reason is the parent is the one that
 * know what action to dispatch. It can be survey, profile, regType, session or whatever that create this input field and knows
 * what action to dispatch.
 */
import React from 'react';
import ReactDOM from 'react-dom';

class Input extends React.Component {
    constructor(props) {
        super(props);
        this.valueChange = this.valueChange.bind(this);
        this.select = this.select.bind(this);
        this.checkHandler = this.checkHandler.bind(this);
    }

    /**
     * Handle all value input change
     * Using refs give us more control when we have multiple response options
     */
    valueChange() {
        let newValue = ReactDOM.findDOMNode(this.refs[this.props.id]).value;
        this.props.changeHandler(this.props.id, newValue);
    }

    /**
     * Radio button also need to be handled differently as one question might has multiple radio button to check
     */
    select(e) {
        let newValue = e.target.value;
        this.props.changeHandler(this.props.id, newValue);
    }

    /**
     * checkbox need to be handled differently as checked attribute is either true or false and 1 or 0
     */
    checkHandler(e) {
        let newValue = e.target.checked;
        this.props.changeHandler(this.props.id, newValue ? 1:0);
    }

    render() {

        let input = () => {
            if(this.props.type === 'text'
                    || this.props.type === 'number'
                    || this.props.type === 'email'
                    || this.props.type === 'password') {
                return (
                            <div>
                                <label required={ this.props.required } htmlFor={ this.props.id }>
                                    { this.props.labelName } &nbsp;
                                    {this.props.required && <span style={{color:'red'}}>*</span>}
                                </label>
                                <input
                                    value={ this.props.response}
                                    onChange={ this.valueChange }
                                    ref={ this.props.id }
                                    className= "form-control"
                                    required= { this.props.required }
                                    type={ this.props.type }
                                    id={ this.props.id }
                                    placeholder={ this.props.placeholder }
                                />
                                {this.props.errorMessage && <span>
                                    <div className="alert alert-danger" role="alert">
                                        { this.props.errorMessage }
                                    </div>
                                </span>}

                            </div>
                       );
            } else if (this.props.type === 'checkbox') {
                return (
                            <div>
                                <label>
                                    <input
                                        checked= { this.props.response }
                                        onChange={ this.checkHandler }
                                        type="checkbox"
                                        ref={ this.props.id }
                                    />
                                        &nbsp;&nbsp; { this.props.labelName }
                                        {this.props.required && <span style={{color:'red'}}>&nbsp;*</span>}
                                </label>
                                {this.props.errorMessage && <span>
                                    <div className="alert alert-danger" role="alert">
                                        { this.props.errorMessage }
                                    </div>
                                </span>}
                            </div>
                        );
            } else if (this.props.type === 'radio') {
                let radios = () => {
                    return  this.props.optionValues.map((radioValue) => {
                        return  (
                                    <div className="radio" key={ `${this.props.id}-${radioValue}` }>
                                        <label>
                                            <input
                                                onChange={ this.select }
                                                checked={ this.props.response === radioValue }
                                                value={ radioValue }
                                                type={ this.props.type }
                                                name={ this.props.labelName }
                                                id={ `${this.props.id}-${radioValue}` }
                                            />
                                                &nbsp;&nbsp; { radioValue }
                                        </label>
                                    </div>
                                );
                    });
                };
                return (
                            <div>
                                <div>
                                    <label>
                                        { this.props.labelName }
                                        {this.props.required && <span style={{color:'red'}}>&nbsp;*</span>}
                                    </label>
                                    {this.props.errorMessage && <span>
                                        <div className="alert alert-danger" role="alert">
                                            { this.props.errorMessage }
                                        </div>
                                    </span>}
                                </div>
                                { radios() }
                            </div>
                        );
            } else if (this.props.type === 'select') {
                let options = () => {
                    let optionTags = [];
                    this.props.optionValues.map(option => {
                        optionTags.push(<option value={ option } key={ option }>{ option }</option>);
                    });

                    return optionTags;
                };
                return (
                            <div>
                                {this.props.labelName && <label htmlFor={ this.props.id }>
                                    { this.props.labelName }
                                    {this.props.required && <span style={{color:'red'}}>&nbsp;*</span>}
                                </label>}
                                {this.props.errorMessage && this.props.labelName && <span>
                                    <div className="alert alert-danger" role="alert">
                                        { this.props.errorMessage }
                                    </div>
                                </span>}
                                <select
                                    ref={ this.props.id }
                                    id={ this.props.id }
                                    className="form-control"
                                    value={ this.props.response }
                                    onChange={ this.valueChange }>
                                        { options() }
                                </select>
                            </div>
                        );
            } else if (this.props.type === 'textarea') {
                return (
                            <div>
                                <label>
                                    { this.props.labelName }
                                    {this.props.required && <span style={{color:'red'}}>&nbsp;*</span>}
                                </label>
                                {this.props.errorMessage && <span>
                                    <div className="alert alert-danger" role="alert">
                                        { this.props.errorMessage }
                                    </div>
                                </span>}
                                <textarea
                                    ref={ this.props.id }
                                    onChange={ this.valueChange }
                                    value={ this.props.response }
                                    className="form-control"
                                    rows={ this.props.numberOfLine }></textarea>
                            </div>
                        );
            }
        };

        return (
                    <div style={{...this.props.styles}} className={ this.props.inputRow === 1 ? 'col-sm-12' : this.props.inputRow === 0.5 ? 'col-sm-6' : 'col-sm-4' }>
                        { input() }
                    </div>
                );
    }
}

export default Input;
