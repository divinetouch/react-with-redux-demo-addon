import React from 'react';
import { connect } from 'react-redux';
import CartAPI from '../../cart-api/cartAPI';
import { switchToProfile } from '../../actions/applicationStateAction';
import { moveToNext } from '../../actions/routeAction';
import { STATE_NAME } from '../../constants/constants';

let mapStateToProps = (state) => ({ 
    profiles: state[STATE_NAME.APPLICATION_STATE].profiles,
    cartItems: state[STATE_NAME.CART].cartItems
});

class CheckoutItem extends React.Component { 

    constructor(props) { 
        super(props);
    }

    clickHandler(profile) { 
        this.props.dispatch(switchToProfile(profile)); 
        this.props.dispatch(moveToNext());
    }

    render() { 
        let items = (items) => { 
            return items.map(item => {
                return (
                            <tr key={ item.id }>
                                <td>{ item.name }</td>
                                <td>
                                    { item.quantity }
                                </td>
                                <td>{ item.price }</td>
                            </tr>
                        );
            });
        };

        let savedProfiles = () => {
            if(this.props.profiles.length > 0) {
                return this.props.profiles.map((profile, index) => { 
                    return (
                                <div key={ index } className="panel panel-default">
                                    <div className="panel-heading">
                                        <div className="row">
                                            <div className="col-xs-12">
                                            <div className="col-sm-11 col-xs-10">
                                                { `${profile.firstName} ${profile.lastName}` } (Saved)
                                            </div>
                                            <div className="col-xs-1">
                                                <button onClick={ this.clickHandler.bind(this, profile) } className="btn btn-success">Edit</button>
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="panel-body">
                                        <table className="table table-responsive table-striped table-bordered table-condensed">
                                            <thead>
                                                <tr>
                                                    <th> Item Name </th>
                                                    <th> Quantity </th>
                                                    <th> Price </th> 
                                                </tr>
                                            </thead>
                                            <tbody>
                                                { items(profile.cart.cartItems) }
                                            </tbody>
                                        </table>
                                        <h4 style={{textAlign: 'right'}}>Total: ${CartAPI.getTotal(profile.cart.cartItems)}</h4>
                                    </div>
                                </div>
                            );    
                });
            }
        };

        return (
                    <div>
                        { savedProfiles() }
                    </div>
                );
    }
}

export default connect(mapStateToProps)(CheckoutItem);
