import React from 'react';
import { connect } from 'react-redux';
import Profiles from './profiles';
import { STATE_NAME } from '../../../constants/constants';

let mapStateToProps = (state) => ({ 
    profiles: state[STATE_NAME.APPLICATION_STATE].profiles
});

class ProfileSidebar extends React.Component { 
    render() { 
        return (
                    <div>
                        {this.props.profiles.length > 0 && <div className="panel panel-default">
                            <div className="panel-heading">
                                Attendees
                            </div>
                            <div className="panel-body">
                                <Profiles profiles={ this.props.profiles }/>
                            </div>
                        </div>}
                    </div>
                );
    }
}

export default connect(mapStateToProps)(ProfileSidebar);
