import React from 'react';
import Profile from './profile';
import ProfileModalView from '../../modal/profileModalView';

class Profiles extends React.Component { 

    constructor(props) { 
        super(props);
        this.state = { 
            profile: undefined,
            showModal: false
        };
    }

    clickHandler(profile) { 
        this.setState({ 
            profile: profile,
            showModal: true
        });
    }

    closeModal() { 
        this.setState({ 
            showModal: false
        });
    }

    render() { 
        let profiles = this.props.profiles.map((profile, index) => { 
            return <Profile key={ index } profile={ profile } clickHandler={ this.clickHandler.bind(this) } />;
        });
        return (
                    <div>
                        <table className="table table-condensed table-bordered table-hover">
                            <tbody>
                                { profiles }
                            </tbody>
                        </table>
                        { this.state.showModal ? <div>
                            <ProfileModalView profile={ this.state.profile } clickHandler={ this.closeModal.bind(this) } />
                        </div>: null }
                    </div>
                );
    }
}

export default Profiles;
