import React from 'react';

export default class Profile extends React.Component { 
    clickHandler () { 
        this.props.clickHandler(this.props.profile);
    }
    render() {
        return (
                    <tr style={{cursor: 'pointer' }} onClick={ this.clickHandler.bind(this) }>
                        <td>
                            <span className="glyphicon glyphicon-align-left glyphicon-modal-window" aria-hidden="true">
                            </span>&nbsp;&nbsp;
                            { `${this.props.profile.firstName} ${this.props.profile.lastName}` }
                        </td>
                    </tr>
                );
    }
}
