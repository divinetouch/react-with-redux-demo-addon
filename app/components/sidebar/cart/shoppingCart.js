import React from 'react';
import { connect } from 'react-redux';
import CartItems from './cart-items';
import CartAPI from '../../../cart-api/cartAPI';
import { changeRoute } from '../../../actions/routeAction';
import { STATE_NAME } from '../../../constants/constants';

let mapStateToProps = (state) => ({ 
    cartItems: state[STATE_NAME.CART].cartItems
});

class ShoppingCart extends React.Component { 
    constructor (props) { 
        super(props);
        this.transitionTo = this.transitionTo.bind(this);
    }
    transitionTo() { 
        this.props.dispatch(changeRoute('checkout'));
    }
    render() {
        return (
                    <div>
                        {this.props.cartItems.length > 0 && <div className="panel panel-default">
                                <div className="panel-heading">
                                    Shopping Cart <span className="glyphicon glyphicon-tags"></span>
                                </div>
                                <div className="panel-body">
                                    <CartItems cartItems= { this.props.cartItems }/>
                                    <h4 style={{textAlign: 'right'}}>Total: ${CartAPI.getTotal(this.props.cartItems)}</h4>
                                </div>
                        </div>}
                    </div>
                );
    }
}

export default connect(mapStateToProps)(ShoppingCart);
