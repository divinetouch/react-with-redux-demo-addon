import React from 'react';
import CartItem from './cart-item';

class CartItems extends React.Component { 

    render() { 
        let items = this.props.cartItems.map((item, index) => { 
            return <CartItem key = { index } item={ item } />;
        });
        return (
                    <ul className="list-group">
                        { items }
                    </ul>
                );
    }
}

export default CartItems;
