import React from 'react';
import ProfileSidebar from './profiles/profileSidebar';
import ShoppingCart from './cart/shoppingCart';
import ContactInfo from './contactInfo/contactInfo';

export default() => { 
    return (
                <div>
                    <ProfileSidebar />
                    <ShoppingCart />
                    <ContactInfo />
                </div>
            );
};
