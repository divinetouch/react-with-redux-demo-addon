import React from 'react';
import { connect } from 'react-redux';
import Sessions from './sessions';
import { STATE_NAME } from '../../constants/constants';

let mapStateToProps = (state) => ({ 
    sections: state[STATE_NAME.SESSION_MERCHANDIZE].sections
});

/**
 * There are multiple sections and each section has multiple sessions
 */
let sessionSection = (props) => {
    return props.sections.map((section, index) => { 
        return <Sessions key={ index } sessionsInSection={ section } />;
    });
};

let SessionsDisplay = (props) => {
    return (
                <div>
                    { sessionSection(props) }
                </div>
            ); 
};

export default connect(mapStateToProps)(SessionsDisplay);
