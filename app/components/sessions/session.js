import React from 'react';
import { connect } from 'react-redux';
import Input from '../inputs/input';
import { Link } from 'react-router';

/**
 * this responsible for creating and rendering each row of the session
 * Again we just using the input component that we use in the profile and survey
 */
class Session extends React.Component { 

    constructor(props) { 
        super(props);
        this.changeHandler = this.changeHandler.bind(this);
    }

    changeHandler(id, newValue) { 
        this.props.changeHandler(this.props.session, newValue);
    }

    render() {
        return (<tr key={ this.props.session.id }> 
                    <td style={{ textAlign: 'center'}}><Input 
                            changeHandler= { this.changeHandler }
                            type={ this.props.session.type }
                            optionValues={ this.props.session.value } 
                            id={ this.props.session.id } 
                            styles={ this.props.session.styles } 
                            required={ this.props.session.required }
                            response={ this.props.session.response }
                            inputRow= { this.props.session.row }
                        />
                    </td>
                    <td>{ this.props.session.name }  <Link to={ `/item/${this.props.session.id}` }>(Detail)</Link></td>
                    <td>{ this.props.session.price }</td>
                </tr>);
    }
}

export default connect()(Session);
