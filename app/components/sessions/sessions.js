import React from 'react';
import { connect } from 'react-redux';
import { updateSession} from '../../actions/sessionAction';
import Session from './session';

/**
 * Each section of session has multple sessions
 * This class responsible for creating the table of the sessions in a
 * particular section
 */
class Sessions extends React.Component { 

    constructor(props) { 
        super(props);
        this.changeHandler = this.changeHandler.bind(this);
    }

    changeHandler(item, quantity) { 
        this.props.dispatch(updateSession(item, quantity));
    }

    render() { 
        let head = () => { 
            return (
                        <tr key={ this.props.sessionsInSection.sectionName }>
                            <th className="col-sm-2">{ this.props.sessionsInSection.columnsName[0] }</th>
                            <th className="col-sm-8">{ this.props.sessionsInSection.columnsName[1] }</th>
                            <th className="col-sm-1">{ this.props.sessionsInSection.columnsName[2] }</th>
                        </tr>
                    );
        };

        let body = () => { 
            return this.props.sessionsInSection.inputs.map((session, index) => { 
                return (<Session key={ index } session={ session } changeHandler = { this.changeHandler } />);
            });
        };

        return (
                    <div className="panel panel-default">
                        <div className="panel-heading">
                            { this.props.sessionsInSection.sectionName }
                        </div>
                        <div className="panel-body">
                            <table className="table table- stiped table-bordered table-condensed table-hover">
                                <thead>
                                    { head() }
                                </thead>
                                <tbody>
                                    { body() }
                                </tbody>
                            </table>
                        </div>
                    </div>
                );
    }
}

export default connect()(Sessions);
