import React from 'react';
import ReactDOM from 'react-dom';
import $ from 'jquery';
import BreadCrumb from './navigation/breadcrumb/breadcrumb';
import { changeRoute } from '../actions/routeAction';
import { connect } from 'react-redux';
import Sidebar from './sidebar/sidebar';
import { STATE_NAME } from '../constants/constants';

let mapStateToProps = (state) => ({
    errorMessage: state[STATE_NAME.APPLICATION_STATE].errorMessage,
    applicationIsReady: state[STATE_NAME.APPLICATION_STATE].applicationIsReady
});

class Template extends React.Component {
    constructor(props) {
        super(props);
        this.toggle = this.toggle.bind(this);
        this.transitionTo = this.transitionTo.bind(this);
    }

    transitionTo(e) {
        e.preventDefault();
        this.props.dispatch(changeRoute(e.target.text));
        $(ReactDOM.findDOMNode(this.refs['bs-navbar-collapse'])).hide();
    }

    toggle() {
        $(ReactDOM.findDOMNode(this.refs['bs-navbar-collapse'])).toggle('fast');
    }

    render() {
        return (
                <div className="container">
                    <div className="row">
                        <nav className="navbar navbar-inverse">
                            <div className="container-fluid">
                                <div className="navbar-header">
                                    <button type="button" onClick={ this.toggle } className="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-navbar-collpase" aria-expanded="false">
                                        <span className="icon-bar"></span>
                                        <span className="icon-bar"></span>
                                        <span className="icon-bar"></span>
                                    </button>
                                    <a className="navbar-brand" href="#" value={'registration'} onClick={ this.transitionTo }>Brand</a>
                                </div>
                                <div className="collapse navbar-collapse" ref="bs-navbar-collapse" id="bs-navbar-collapse">
                                    <ul className="nav navbar-nav navbar-right">
                                        <li value={'registration'} onClick={this.transitionTo}><a href="#">Home</a></li>
                                        <li value={'about'} onClick={this.transitionTo}><a href="#">About</a></li>
                                        <li value={'contact'} onClick={this.transitionTo}><a href="#">Contact</a></li>
                                    </ul>
                                </div>
                            </div>
                        </nav>
                    </div>
                    <div className="row">
                        <div className="jumbotron" style={{textAlign: 'center'}}>
                            <h2>Welcome to React with React-Redux Demo</h2>
                        </div>
                        {this.props.applicationIsReady ? <BreadCrumb /> : null }
                        <div className="row">
                            <div className="col-sm-9">
                                {this.props.errorMessage && <div
                                    style={{  textAlign: 'center', fontSize: '1.3em'}}
                                    className="alert alert-danger" role="alert">
                                    <span>
                                        { this.props.errorMessage }
                                    </span></div>
                                }
                            </div>
                            <div className="col-sm-3">
                                &nbsp;
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-sm-12 col-md-9">
                                {this.props.children}
                            </div>
                            {this.props.applicationIsReady && <div className="col-md-3 hidden-xs hidden-sm">
                                <Sidebar />
                            </div>}
                        </div>
                        <div className="row" style={{minHight:15}}>
                            &nbsp;
                        </div>
                    </div>
                </div>
            );
    }

}

export default connect(mapStateToProps)(Template);
