import React from 'react';
import { connect } from 'react-redux';
import Input from '../inputs/input';
import {  updateProfileResponse } from '../../actions/profileAction';

class Profile extends React.Component { 

    constructor(props){ 
        super(props);
        this.changeHandler = this.changeHandler.bind(this);
    }

    /**
     * handle change event so the input component does not have to
     * know whether to update profile input, survey input, or any other type of input
     */
    changeHandler(id, newValue){ 
        if(this.props.editable) {
            this.props.dispatch(updateProfileResponse(id, newValue));
        }
    }

    render() {
        /**
         * Generating inputs fields given the all of the profile
         * input within a specific profile section from the state
         */
        let profileInputInSection = (section) => {
            return section.inputs.map(input => { 
                return <Input 
                            changeHandler= { this.changeHandler }
                            key={ input.id }
                            type={ input.type }
                            optionValues={ input.value } 
                            labelName={ input.label } 
                            id={ input.id } 
                            styles={ input.styles } 
                            placeholder={ input.placeholder }
                            required={ input.required }
                            response={ input.response }
                            inputRow={ input.row }
                            numberOfLine={ input.numberOfLine }
                            errorMessage={ input.errorMessage }
                        />;
            });
        };

        /**
         * generate a panel of all the section
         */
        let profileSections = () => { 
            return this.props.profileSections.map(section => { 
                if(section.show) {
                    return (
                                <div key={ section.sectionName }>
                                    <div className="panel panel-default">
                                        <div className="panel-heading">
                                            { section.sectionName }
                                        </div>
                                        <div className="panel-body">
                                            { profileInputInSection(section) }
                                        </div>
                                    </div>
                                </div>
                           );
                }
                    
            });
        };

        return (
                    <form>
                        { profileSections() }
                    </form>
                );
    }
}

export default connect()(Profile);
