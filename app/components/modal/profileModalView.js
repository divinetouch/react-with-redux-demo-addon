import React from 'react';
import ReactDOM from 'react-dom';
import $ from 'jquery';
import Profile from '../../components/profile/profile';
import Survey from '../../components/survey/survey';

class ProfileModalView extends React.Component { 

    componentDidMount() { 
        let profileModalView = ReactDOM.findDOMNode(this.refs['profileModalView']);
        $(profileModalView).modal('show');
        $(profileModalView).on('hidden.bs.modal', this.props.clickHandler);
    }
    
    render() { 
        return (
                    <div className="modal fade" tabIndex="-1" role="dialog" ref="profileModalView">
                        <div className="modal-dialog">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 className="modal-title">{ `${this.props.profile.firstName} ${this.props.profile.lastName}` }</h4>
                                </div>
                                <div className="modal-body">
                                    <div className="row">
                                        <Profile editable={ false } profileSections={ this.props.profile.profileInputs.sections } />
                                        <Survey editable={ false } surveyInputs = { this.props.profile.surveyInputs.inputs } />
                                    </div>
                                </div>
                                <div className="modal-footer">
                                    <button type="button" className="btn btn-success" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                );
    }
}

export default ProfileModalView;
