import React from 'react';
import { connect } from 'react-redux';
import Input from '../inputs/input';
import { updateSurveyResponse } from '../../actions/surveyAction';

class Survey extends React.Component { 

    constructor(props){ 
        super(props);
        this.changeHandler = this.changeHandler.bind(this);
    }

    /**
     * handle change event so the input component does not have to
     * know whether to update profile input, survey input, or any other type of input
     */
    changeHandler(id, newValue){ 
        if(this.props.editable) {
            this.props.dispatch(updateSurveyResponse(id, newValue));
        }
    }

    render() {
        /**
         * Generating inputs fields given the all of the profile
         * input from the state
         */
        let surveyInputsInSection = () => {
            return this.props.surveyInputs.map(input => { 
                return <Input 
                            changeHandler= { this.changeHandler }
                            key={ input.id }
                            type={ input.type }
                            optionValues={ input.value } 
                            labelName={ input.label } 
                            id={ input.id } 
                            styles={ input.styles } 
                            placeholder={ input.placeholder }
                            required={ input.required }
                            response={ input.response }
                            inputRow={ input.row }
                            numberOfLine={ input.numberOfLine }
                            errorMessage={ input.errorMessage }
                        />;
            });
        };
    
        return (
                    <form>
                        { surveyInputsInSection() }
                    </form>
                );
    }
}

export default connect()(Survey);
