import React from 'react';
import RegTypes from './reg-types';
import { connect } from 'react-redux';
import { STATE_NAME } from '../../constants/constants';

/**
 * There are multiple sections and each section has multiple sections
 */
let mapStateToProps = (state) => ({ 
    sections: state[STATE_NAME.REGTYPE_MERCHANDIZE].sections
});

/**
 * Creating sections of reg type
 */
let sections = (props) => {  
    return props.sections.map((section, index) => { 
        return <RegTypes key={ index } regTypesInSection={ section } />;
    });
};

let regTypeSection = (props) => { 
    return (
                <div>
                    { sections(props) }
                </div>
            );
};

export default connect(mapStateToProps)(regTypeSection);
