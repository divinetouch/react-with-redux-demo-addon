import React from 'react';
import { updateRegType } from '../../actions/regTypeAction';
import RegType from './reg-type';
import { connect } from 'react-redux';

/**
 * Each section of regTypes has multple regTypes
 * This class responsible for creating the table of the regTypes in a
 * particular section
 */
class RegTypes extends React.Component { 

    constructor(props) { 
        super(props);
        this.changeHandler = this.changeHandler.bind(this);
    }

    changeHandler(item, quantity) { 
        this.props.dispatch(updateRegType(item, quantity));
    }

    render() { 
        let head = () => { 
            return (
                        <tr key={ this.props.regTypesInSection.sectionName }>
                            <th className="col-sm-2">{ this.props.regTypesInSection.columnsName[0] }</th>
                            <th className="col-sm-8">{ this.props.regTypesInSection.columnsName[1] }</th>
                            <th className="col-sm-1">{ this.props.regTypesInSection.columnsName[2] }</th>
                        </tr>
                    );
        };

        let body = () => { 
            return this.props.regTypesInSection.inputs.map((regType, index) => { 
                return (<RegType key={ index } regType={ regType } changeHandler = { this.changeHandler } />);
            });
        };

        return (
                    <div className="panel panel-default">
                        <div className="panel-heading">
                            { this.props.regTypesInSection.sectionName }
                        </div>
                        <div className="panel-body">
                            <table className="table table-stiped table-bordered condensed table-hover">
                                <thead>
                                    { head() }
                                </thead>
                                <tbody>
                                    { body() }
                                </tbody>
                            </table>
                        </div>
                    </div>
                );
    }
}

export default connect()(RegTypes);
