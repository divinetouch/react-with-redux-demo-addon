import React from 'react';
import { connect } from 'react-redux';
import Input from '../inputs/input';
import { Link } from 'react-router';

class RegType extends React.Component { 

    constructor(props) { 
        super(props);
        this.changeHandler = this.changeHandler.bind(this);
    }

    changeHandler(id, newValue) { 
        this.props.changeHandler(this.props.regType, newValue);
    }

    render() {
        return (<tr key={ this.props.regType.id }> 
                    <td style={{ textAlign: 'center'}}><Input 
                            changeHandler= { this.changeHandler }
                            type={ this.props.regType.type }
                            optionValues={ this.props.regType.value } 
                            id={ this.props.regType.id } 
                            styles={ this.props.regType.styles } 
                            required={ this.props.regType.required }
                            response={ this.props.regType.response }
                            inputRow= { this.props.regType.row }
                        />
                    </td>
                    <td>{ this.props.regType.name } <Link to={ `/item/${this.props.regType.id}` }>(Detail)</Link></td>
                    <td>{ this.props.regType.price }</td>
                </tr>);
    }
}

export default connect()(RegType);
