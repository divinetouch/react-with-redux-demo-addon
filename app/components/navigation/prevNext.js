import React from 'react';
import { connect } from 'react-redux';
import { moveToPrevious } from '../../actions/routeAction';

let PrevAndNext = React.createClass({ 
    moveToNext() { 
        this.props.nextHandler();
    },

    moveToPrevious() { 
        this.props.dispatch(moveToPrevious());
    },
    
    render() { 
        return (
                <div>
                        <div>
                            <button style={{ marginTop: 5}} onClick={ this.moveToPrevious } className="btn btn-success col-md-3 col-xs-12" to='/'>
                                <span className="glyphicon glyphicon-arrow-left"></span>
                                &nbsp;&nbsp;Previous
                            </button>
                            <button style={{ marginTop:5 }}  onClick={ this.props.nextHandler } className="btn btn-success col-md-offset-6 col-md-3 col-xs-12" to='/survey'>
                                Next&nbsp;&nbsp;<span className="glyphicon glyphicon-arrow-right">
                                </span>
                            </button>
                        </div>
                </div>
            );
    }
});

export default connect()(PrevAndNext);
