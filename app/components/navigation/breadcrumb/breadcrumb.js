import React from 'react';
import { connect } from 'react-redux';
import { changeRoute } from '../../../actions/routeAction';
import BreadcrumbLink from './breadcrumbLink';
import { STATE_NAME } from '../../../constants/constants';

let mapStateToProps = (state) => ({ 
    applicationRoute: state[STATE_NAME.APPLICATION_ROUTE].paths
});

class BreadCrumb extends React.Component { 
    constructor(props) { 
        super(props);
    }

    transitionTo(route) { 
        if(!route.disabled) {
            this.props.dispatch(changeRoute(route.path.toLowerCase()));
        }
    }


    render() { 
        let links = () => {
            let applicationRoute = this.props.applicationRoute;
            return applicationRoute.map((route, index) => { 
                //only show the breadcrumb links
                if(route.breadCrumb){
                    return (
                                <BreadcrumbLink key={ index } clickHandler={ this.transitionTo.bind(this) } route={ route }/>
                           );
                }
            });
        };
        return (
                    <ol className="breadcrumb">
                        { links() }
                    </ol>
                );
    }
}

export default connect(mapStateToProps)(BreadCrumb);
