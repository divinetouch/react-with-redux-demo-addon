import React from 'react';

export default class BreadcrumbLink extends React.Component {
    constructor(props) {
        super(props);
    }

    clickHandler(e) {
        e.preventDefault();
        this.props.clickHandler(this.props.route);
    }

    render() {
        let style = undefined;
        if(this.props.route.disabled) {
            style = {
                color: 'darkgrey',
                cursor: 'default'
            };
        }
        return (

                    <li key={ this.props.route.path } className={ this.props.route.active ? 'active':''} disabled ={ this.props.route.disabled }>
                        <a href="#"
                            style={style}
                            className="btn"
                            onClick={ this.clickHandler.bind(this) }>
                            { this.props.route.path }
                        </a>
                    </li>
                );
    }
}
