import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import { STATE_NAME } from '../constants/constants';

let mapStateToProps = (state) => ({ 
    regTypeItems: state[STATE_NAME.REGTYPE_MERCHANDIZE].sections,
    sessionItems: state[STATE_NAME.SESSION_MERCHANDIZE].sections
});

let searchItem = (props) => { 
    let joinItems = props.regTypeItems.concat(props.sessionItems);
    let items = [];
    joinItems.forEach(item => { 
        item.inputs.forEach(input => { 
            items.push(input); 
        });
    }); 
    return items.find(item => String(item.id) === props.params.id);
};

class Item extends React.Component { 
    render() { 
        let item = searchItem(this.props);
        return (
                    <div className="row">
                        <div className="col-sm-4">
                            <img src="http://placehold.it/250x250" />
                        </div>
                        <div className="col-sm-12" style={{paddingTop:5}}>
                            <ul className="list-group">
                                <li className="list-group-item">ID: { item.id }</li>
                                <li className="list-group-item">Name: { item.name }</li>
                                <li className="list-group-item">Description: { item.description }</li>
                                <li className="list-group-item">Price: { item.price }</li>
                            </ul>
                        </div>
                        <div className="col-sm-12">
                            <Link to="/" className="btn btn-success"><span className="glyphicon glyphicon-arrow-left"></span> Go back</Link>
                        </div>
                    </div>
                );
    }    
}

export default connect(mapStateToProps)(Item);
