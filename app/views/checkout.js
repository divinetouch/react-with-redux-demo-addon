import React from 'react';
import { connect } from 'react-redux';
import CheckoutItem from '../components/checkout/checkout-item';
import { moveToNext } from '../actions/routeAction';
import ViewsMixin from './viewsMixin';
import { STATE_NAME } from '../constants/constants';

let mapStateToProps = (state) => ({ 
    cartItems: state[STATE_NAME.CART].cartItems,
    profileSections: state[STATE_NAME.PROFILE_INPUTS].sections,
    applicationRoute: state[STATE_NAME.APPLICATION_ROUTE].paths,
    currentRoute: state.routing.path
});

class Checkout extends ViewsMixin { 
    constructor(props) { 
        super(props);
        this.transitionTo = this.transitionTo.bind(this);
    }

    transitionTo() { 
        this.props.dispatch(moveToNext());
    }

    render() { 
        return (
                    <div>
                        <CheckoutItem />
                        <div style={{textAlign: 'right'}}>
                            <button onClick={ this.transitionTo } className="btn btn-success">Register Next Attendee</button>
                        </div>
                    </div>
                );
    }
}

export default connect(mapStateToProps)(Checkout);
