import React from 'react';
import Profile from '../components/profile/profile';
import { connect } from 'react-redux';
import { showSectionView } from '../actions/profileAction';
import { validateProfileInputsAndMoveNext } from '../actions/routeAction.js';
import PrevAndNext from '../components/navigation/prevNext';
import ViewsMixin from './viewsMixin';
import { STATE_NAME } from '../constants/constants';

let mapStateToProps = (state) => ({ 
    profileSections: state[STATE_NAME.PROFILE_INPUTS].sections,
    currentRoute: state.routing.path
});

/**
 * Handling showing the profile and nothing else
 * the profile component now handling all the inputs
 * and capturing the response
 */
class ProfilePage extends ViewsMixin{
    constructor (props) { 
        super(props);
        this.validateProfileInput = this.validateProfileInput.bind(this);
    }
    /**
     * We want to show every section of the profile
     */
    componentWillMount() { 
        super.componentWillMount();
        this.props.profileSections.forEach(section => this.props.dispatch(showSectionView(section.sectionName)));
    }

    validateProfileInput() { 
        this.props.dispatch(validateProfileInputsAndMoveNext());
    }

    render() {
        return (
                <div>
                    <Profile editable={ true } profileSections={ this.props.profileSections }/>
                    <PrevAndNext nextHandler={ this.validateProfileInput }/>
                </div>
            );
    }
}

export default connect(mapStateToProps)(ProfilePage);
