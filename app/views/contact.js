import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { incrementCounter, decrementCounter } from '../actions/counterAction';
import { STATE_NAME } from '../constants/constants';
import ViewsMixin from './viewsMixin';

let mapStateToProps = (state) => ({ 
    counter: state[STATE_NAME.COUNTER],
    currentRoute: state.routing.path
});

let mapDispatchToProps = (dispatch) => ({ 
    actionInc: bindActionCreators(incrementCounter, dispatch),
    actionDec: bindActionCreators(decrementCounter, dispatch),
    dispatch: dispatch
});

class Contact extends ViewsMixin {
    constructor(props) { 
        super(props);
    }

    render() {
        console.log(this.props.counter.count);
        return (
                    <div style={{textAlign: 'center'}}>
                        <h2>Instead of cotacting me, why don't we test counting instead</h2>
                        <div className="row">
                            <div className="col-sm-4 col-sm-offset-4" style={{textAlign:'center'}}>
                                <button className="btn btn-success" onClick={ this.props.actionInc }>Increment</button>
                                &nbsp;
                                <button className="btn btn-success" onClick={ this.props.actionDec }>Decrement</button>
                            </div>
                        </div>
                        <div className="row" style={{paddingTop:10}}>
                            <div className="col-sm-8 col-sm-offset-2">
                                <div className="panel panel-default">
                                    <div className="panel-heading">
                                        Counter Value
                                    </div>
                                    <div className="panel-body">
                                        <div style={{textAlign:'center'}}>
                                            <h1>{ this.props.counter.count }</h1>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Contact);
