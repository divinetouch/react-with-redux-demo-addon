import React from 'react';
import { updateActiveRoute, changeRoute } from '../actions/routeAction';

export default class ViewsMixin extends React.Component { 

    componentWillMount() { 
        /**
         * the default landing page is registration page
         */
        if(this.props.currentRoute === '/' || this.props.currentRoute === undefined) {
            this.props.dispatch(changeRoute('registration'));
        } else { 
            this.props.dispatch(updateActiveRoute(this.props.currentRoute));
        }
    }
}
