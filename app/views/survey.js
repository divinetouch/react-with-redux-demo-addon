import React from 'react';
import Survey from '../components/survey/survey';
import { connect } from 'react-redux';
import { validateSurveyInputsAndMoveNext } from '../actions/routeAction';
import PrevAndNext from '../components/navigation/prevNext';
import ViewsMixin from './viewsMixin';
import { STATE_NAME } from '../constants/constants';

let mapStateToProps = (state) => ({ 
    applicationState: state[STATE_NAME.APPLICATION_STATE],
    surveyInputs: state[STATE_NAME.SURVEY_INPUTS].inputs,
    currentRoute: state.routing.path
});

class SurveyPage extends ViewsMixin {
    
    constructor (props) { 
        super(props);
        this.validateSurveyInput = this.validateSurveyInput.bind(this);
    }

    validateSurveyInput() { 
        this.props.dispatch(validateSurveyInputsAndMoveNext());
    }

    render() {
        return (
                    <div>
                        <div className="panel panel-default">
                            <div className="panel-heading">
                                Please answwer the following survey questions
                            </div>
                            <div className="panel-body">
                                <Survey editable={ true } surveyInputs = { this.props.surveyInputs }/>
                            </div>
                        </div>
                        <PrevAndNext nextHandler= { this.validateSurveyInput }/>
                    </div>
                );
    }
}

export default connect(mapStateToProps)(SurveyPage);
