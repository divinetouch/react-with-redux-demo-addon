import React from 'react';
import { connect } from 'react-redux';
import ViewsMixin from './viewsMixin';

let mapStateToProps = (state) => ({ 
    currentRoute: state.routing.path
});

class About extends ViewsMixin {
    render() {
        return (
                    <div>
                        <h5>
                            Thank you for checking this demo, this is just a simple demo site that I created to learn React.
                            Hopefully, you find it useful. 
                        </h5>
                        <div className="col-sm-12">
                            <p>
                                To learn more about React framework please visit: <a href="//facebook.github.io/react/" target="_blank">React</a>
                            </p>
                            <p>
                                To learn more about redux please visit: <a href="//redux.js.org/" target="_blank">Redux</a>
                            </p>
                            <p>
                                To learn more about react-redux please visit: <a href="//github.com/rackt/react-redux" target="_blank">React Redux</a>
                            </p>
                            <p>
                                To learn more about react-router please visit: <a href="//github.com/rackt/react-router" target="_blank">React Router</a>
                            </p>
                        </div>
                        <h5>Here are some more useful resources that might help you in understanding the concept behind Redux 
                            and functional programing in general
                        </h5>
                        <div className="col-sm-12">
                            <p>
                                A free course by Dan Abramov, the guy who create Redux: <a href="//egghead.io/series/getting-started-with-redux" target="_blank">egghead.io course</a>
                            </p>
                            <p>
                                A great and free book about functional programing: <a href="//www.gitbook.com/book/drboolean/mostly-adequate-guide/details" target="_blank">Mostly Adequatee Guide to Functional Programming</a>
                            </p>
                        </div>
                        <div className="col-sm-12" style={{textAlign: 'center', paddingTop: 15}}>
                            <h5>Watch Dan Abramov, the creator of Redux</h5>
                            <div className="embed-responsive embed-responsive-16by9">
                                <iframe className="embed-resposive-item" src="https://www.youtube.com/embed/xsSnOQynTHs" frameborder="0" allowfullscreen></iframe>
                            </div>
                        </div>
                        <div className="col-sm-12" style={{textAlign: 'center', paddingTop: 15}}>
                            <h5>Watch Pete Hunt, the lead developer of React where he talked about Virtual DOM and why Angular is still a main contender</h5>
                            <div className="embed-responsive embed-responsive-16by9">
                                <iframe className="embed-responsive-item" src="https://www.youtube.com/embed/-DX3vJiqxm4" frameborder="0" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                );
    }
}

export default connect(mapStateToProps)(About);
