import React from 'react';
import { connect } from 'react-redux';
import SessionsSections from '../components/sessions/sessionsSections';
import RegTypesSections from '../components/reg-types/reg-type-sections';
import LoadingModalView from '../components/modal/loadingModalView';
import { validateRegTypeAndMoveNext } from '../actions/routeAction';
import ViewsMixin from './viewsMixin';
import { STATE_NAME } from '../constants/constants';
import { loadInitialDataFromServer } from '../actions/loadDataFromServerAction';

/**
 * we want to get the whole state tree just for testing purposes. 
 * This is not recomended as it can be big.
 * So try to get only the part where the component needs. Please see CartItems as an example.
 */
//paranthesize the body to return an object literal expression
let mapStateToProps = (state) => ({ 
    applicationIsReady: state[STATE_NAME.APPLICATION_STATE].applicationIsReady,
    currentRoute: state.routing.path
});

/**
 * Instead of using the new ES6 syntax above, this is also correct
 */
// function mapStateToProps(state){ 
//     return {reduxState: state};
// };

class Registration extends ViewsMixin {
    constructor(props) { 
        super(props);
        this.transitionNext = this.transitionNext.bind(this);
    }

    transitionNext() { 
        this.props.dispatch(validateRegTypeAndMoveNext());
    }

    componentWillMount() { 
        super.componentWillMount();
        this.props.dispatch(loadInitialDataFromServer()); //TODO: not yet implemented, just a setTimeout for now
    }

    render() {
        return (
                    <div>
                            {this.props.applicationIsReady ? <div>
                                <RegTypesSections />
                                <SessionsSections />
                                <button className="btn btn-success col-sm-offset-9 col-sm-3 col-xs-12" onClick={ this.transitionNext }>
                                    Next <span className="glyphicon glyphicon-arrow-right"></span>
                                </button>
                            </div>
                            :
                            <div ref='loadingModalView'>
                                <LoadingModalView  title="Simulate Loading Data From Server"/>
                            </div>
                            }
                    </div>
                );
    }
}

export default connect(mapStateToProps)(Registration);
