/**
 * Without using ES6 class and does not take advantage of redux state
 * This means lots of logic in the component itself
 */
import React from 'react';
import { connect } from 'react-redux';
import { login } from '../actions/loginAction';
import LoadingModalView from '../components/modal/loadingModalView';
import Input from '../components/inputs/input';
import { STATE_NAME } from '../constants/constants';

let mapStateToProps = (state) => ({ 
    applicationIsReady: state[STATE_NAME.APPLICATION_STATE].applicationIsReady
});

let Login = React.createClass({ 

    getInitialState: function() { 
        return { 
            webId: undefined,
            lastName: undefined,
            email: undefined,
            activeTab: undefined,
            isValidating: this.props.applicationIsReady,
            tabs: [ 
                {
                    name: 'With Email'
                },
                {
                    name: 'With Web Id'
                }
            ]
        };
    },

    componentDidMount() { 
        this.setState({ 
            activeTab: this.state.tabs[0].name
        });

    },

    changeHandler(id, newValue) { 
        this.setState({ 
            email: id==='email' ? newValue: this.state.email,
            lastName: id==='lastName' ? newValue: this.state.lastName,
            webId: id==='webId' ? newValue: this.state.webId
        });
    },

    submit() { 
        this.setState({ 
            isValidating: true
        });
        this.props.dispatch(login(this.state));
    },

    componentWillReceiveProps(newProps) { 
        this.setState({ 
            isValidating: !newProps.applicationIsReady
        });
    },

    switchTab (e) { 
        e.preventDefault();
        let index = this.state.tabs.findIndex(tab => tab.name === e.target.text);
        this.setState({ 
            activeTab: this.state.tabs[index].name
        });
    },

    login: function() { 
        this.props.dispatch(login({ webId: this.state.webId, lastName: this.state.lastName, email: this.state.email, password: this.state.password  }));
    },
    
    render: function() { 
        let tabs = () => {
            return this.state.tabs.map(tab => { 
                return (
                        <li key={ tab.name } className={ this.state.activeTab === tab.name ? 'active':'' } onClick={ this.switchTab } value={ name }>
                            <a href="#">
                                { tab.name }
                            </a>
                        </li>
                       );
            });
        };
        return (
                    <div className="container">
                        <div className="row">
                            <div className="col-xs-12 col-md-6 col-md-offset-3">
                                <div className="panel panel-primary">
                                    <div className="panel-heading">
                                        Log In
                                    </div>
                                    <div className="panel-body">
                                        <ul className="nav nav-tabs">
                                            { tabs() }
                                        </ul>
                                        {this.state.tabs[0].name === this.state.activeTab && <div>
                                            <div style={{ marginTop: 20 }}> 
                                                <Input 
                                                    changeHandler= { this.changeHandler }
                                                    key= "email"
                                                    type= "email"
                                                    labelName= "email"
                                                    id= "email"
                                                    placeholder= "Email Address"
                                                    required= { true }
                                                    response={ this.state.email }
                                                    inputRow= { 1 }
                                                    errorMessage={ this.state.errorMessage }
                                                />
                                                <Input 
                                                    changeHandler= { this.changeHandler }
                                                    key= "lastName"
                                                    type= "text"
                                                    labelName= "lastName"
                                                    id= "lastName"
                                                    placeholder= "Last Name"
                                                    styles={{ paddingTop: 10 }}
                                                    required= { true }
                                                    response={ this.state.lastName }
                                                    inputRow= { 1 }
                                                    errorMessage={ this.state.errorMessage }
                                                />
                                                <div className="col-sm-12" style={{marginTop: 10}}>
                                                    <button onClick={ this.submit } type="button" className="btn btn-primary">Log In</button>
                                                </div>
                                            </div>
                                        </div> }
                                        {this.state.tabs[1].name === this.state.activeTab && <div>
                                            <div style={{ marginTop: 20 }}> 
                                                <Input 
                                                    changeHandler= { this.changeHandler }
                                                    key= "webId"
                                                    type= "text"
                                                    labelName= "Web ID"
                                                    id= "webId"
                                                    placeholder= "Web ID"
                                                    required= { true }
                                                    response={ this.state.webId }
                                                    inputRow= { 1 }
                                                    errorMessage={ this.state.errorMessage }
                                                />
                                                <Input 
                                                    changeHandler= { this.changeHandler }
                                                    key= "lastName"
                                                    type= "text"
                                                    labelName= "lastName"
                                                    id= "lastName"
                                                    placeholder= "Last Name"
                                                    styles={{ paddingTop: 10 }}
                                                    required= { true }
                                                    response={ this.state.lastName }
                                                    inputRow= { 1 }
                                                    errorMessage={ this.state.errorMessage }
                                                />
                                                <div className="col-sm-12" style={{ paddingTop: 15, textAlign: 'center'}}>
                                                    <label>Or</label>
                                                </div>
                                                <Input 
                                                    changeHandler= { this.changeHandler }
                                                    key= "email"
                                                    type= "email"
                                                    labelName= "email"
                                                    id= "email"
                                                    placeholder= "email"
                                                    required= { true }
                                                    response={ this.state.email }
                                                    inputRow= { 1 }
                                                    errorMessage={ this.state.errorMessage }
                                                />
                                                <div className="col-sm-12" style={{marginTop: 10}}>
                                                    <button onClick={ this.submit} type="button" className="btn btn-primary">Log In</button>
                                                </div>
                                            </div>
                                        </div> }
                                    </div>
                                </div>
                            </div>
                        </div>
                        {this.state.isValidating && <div>
                            <LoadingModalView title="Simulate Authenticating a User" />
                        </div>}
                    </div>
                    
                );
    }
});

export default connect(mapStateToProps)(Login);
