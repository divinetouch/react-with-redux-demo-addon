import React from 'react';
import { connect } from 'react-redux';
import Profile from '../components/profile/profile';
import Survey from '../components/survey/survey';
import { hideSectionView } from '../actions/profileAction';
import PrevAndNext from '../components/navigation/prevNext';
import { validateAllInputsAndMoveNext } from '../actions/routeAction';
import ViewsMixin from './viewsMixin';
import { STATE_NAME } from '../constants/constants';

let mapStateToProps = (state) => ({ 
    cartItems: state[STATE_NAME.CART].cartItems,
    profileSections: state[STATE_NAME.PROFILE_INPUTS].sections,
    surveyInputs: state[STATE_NAME.SURVEY_INPUTS].inputs,
    currentRoute: state.routing.path
});

class Recap extends ViewsMixin { 
    constructor(props) { 
        super(props);
        this.validateAllInputs = this.validateAllInputs.bind(this);
    }

    /**
     * we want to hide a specific section of the profile
     * Again, this can be part of state and values are from the server
     */
    componentWillMount() { 
        super.componentWillMount();
        this.props.dispatch(hideSectionView('extra'));
    }

    validateAllInputs() { 
        this.props.dispatch(validateAllInputsAndMoveNext());
    }

    render() { 
        return (
                    <div>
                        <div className="panel panel-default">
                            <div className="panel-heading">
                                Profile Recap
                            </div>
                            <div className="panel-body">
                                <Profile profileSections={ this.props.profileSections }/>
                            </div>
                        </div>
                        <div className="panel panel-default">
                            <div className="panel-heading">
                                Survey Recap
                            </div>
                            <div className="panel-body">
                                <Survey surveyInputs = { this.props.surveyInputs }/>
                            </div>
                        </div>
                        <PrevAndNext nextHandler={ this.validateAllInputs } />
                    </div>
                );
    }
}

export default connect(mapStateToProps)(Recap);
