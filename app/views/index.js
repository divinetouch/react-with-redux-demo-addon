import About from './about';
import Item from './item';
import Profile from './profile';
import Recap from './recap';
import Survey from './survey';
import Checkout from './checkout';
import Contact from './contact';
import Registration from './registration';
import Login from './login';
import NotFound from './404';

export { About, Item, Profile, Recap, Survey, Checkout, Contact, Registration, Login, NotFound };
