//BOOTSTRAP IMPORT
global.$ = global.jQuery = require('jquery');
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.min.js';
// BOOTSTRAP IMPORT END

import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import reducers from './reducers/reducers';
import thunk from 'redux-thunk';
import { logger } from './logger/logger';
import { syncReduxAndRouter } from 'redux-simple-router';
import createBrowserHistory from 'history/lib/createBrowserHistory';

import { About, Contact, Item, Checkout, Survey, Profile, Recap, Registration, Login, NotFound } from './views';
import Template from './components/template';
import {Router, Route, IndexRoute} from 'react-router';

const store = applyMiddleware(logger, thunk)(createStore)(reducers);
const history = createBrowserHistory();
syncReduxAndRouter(history, store);

ReactDOM.render(
        <Provider store={ store }>
            <Router history={history}>
                <Route path="/" component={Template}>
                    <IndexRoute component={Registration} />
                    <Route path="about" component={About} />
                    <Route path="contact" component={Contact} />
                    <Route path="item/:id" component={Item} />
                    <Route path="checkout" component={Checkout} />
                    <Route path="profile" component={Profile} />
                    <Route path="recap" component={Recap} />
                    <Route path="survey" component={Survey} />
                    <Route path="registration" component={Registration} />
                    <Route path="login" component={ Login } />
                    <Route path="/*" component={ NotFound } />
                </Route>
            </Router>
        </Provider>,
        document.getElementById('app')
    );
