export const testData = { 
    sections: [
        {
            sectionName: 'Personal Information',
            show: true,
            inputs: [
                { 
                    id: 1,
                    label: 'First Name',
                    type: 'text',
                    placeholder: 'First name',
                    styles: {
                        paddingTop: 10,
                        color: 'black'
                    },
                    response: undefined,
                    required: true,
                    row: 0.33
                },
                { 
                    id: 2,
                    label: 'MI',
                    type: 'text',
                    placeholder: 'Middle Initial',
                    styles: {
                        paddingTop: 10,
                        color: 'black'
                    },
                    response: undefined,
                    required: false,
                    row: 0.33
                },
                { 
                    id: 3,
                    label: 'Last Name',
                    type: 'text',
                    placeholder: 'Last Name',
                    styles: {
                        paddingTop: 10,
                        color: 'black'
                    },
                    response: undefined,
                    required: true,
                    row: 0.33
                },
                { 
                    id: 4,
                    label: 'Address Line 1',
                    type: 'text',
                    placeholder: 'address Line 1',
                    styles: {
                        paddingTop: 10,
                        color: 'green'
                    },
                    response: undefined,
                    required: true,
                    row: 1
                },
                { 
                    id: 5,
                    label: 'Phone Number',
                    type: 'number',
                    placeholder: 'Phone number',
                    styles: {
                        paddingTop: 10
                    },
                    response: undefined,
                    required: true,
                    row: 0.5
                },
                { 
                    id: 666666,
                    label: 'Email',
                    type: 'email',
                    placeholder: 'Personal Email',
                    styles: {
                        paddingTop: 10
                    },
                    response: undefined,
                    required: true,
                    row: 0.5
                }
            ]
        },
        { 
            sectionName: 'Company Information',
            show: true,
            inputs: [
                { 
                    id: 7,
                    label: 'Company Name',
                    type: 'text',
                    placeholder: 'Company Name',
                    styles: {
                        paddingTop: 10,
                        color: 'green'
                    },
                    response: undefined,
                    required: true,
                    row: 1
                },
                { 
                    id: 8,
                    label: 'Company Address',
                    type: 'text',
                    placeholder: 'address Line 1',
                    styles: {
                        paddingTop: 10,
                        color: 'green'
                    },
                    response: undefined,
                    required: true,
                    row: 1
                },
                { 
                    id: 9,
                    label: 'Zip Code',
                    type: 'number',
                    placeholder: 'zip code',
                    styles: {
                        paddingTop: 10,
                        color: 'green'
                    },
                    response: undefined,
                    required: true,
                    row: 0.33
                },
                { 
                    id: 10,
                    label: 'State',
                    type: 'text',
                    placeholder: 'State Name',
                    styles: {
                        paddingTop: 10,
                        color: 'green'
                    },
                    response: undefined,
                    required: true,
                    row: 0.33
                },
                { 
                    id: 11,
                    label: 'Country',
                    type: 'text',
                    placeholder: 'Country Name',
                    styles: {
                        paddingTop: 10,
                        color: 'green'
                    },
                    response: undefined,
                    required: true,
                    row: 0.33
                },
                { 
                    id: 12,
                    label: 'Phone Number',
                    type: 'number',
                    placeholder: 'Phone number',
                    styles: {
                        paddingTop: 10
                    },
                    response: undefined,
                    required: true,
                    row: 0.5
                },
                { 
                    id: 13,
                    label: 'Email',
                    type: 'email',
                    placeholder: 'Work Email',
                    styles: {
                        paddingTop: 10
                    },
                    response: undefined,
                    required: true,
                    row: 0.5
                }
            ]
        },
        { 
            sectionName: 'Home Information',
            show: true,
            inputs: [
                { 
                    id: 14,
                    label: 'Company Name',
                    type: 'text',
                    placeholder: 'Company Name',
                    styles: {
                        paddingTop: 10,
                        color: 'green'
                    },
                    response: undefined,
                    required: true,
                    row: 1
                },
                { 
                    id: 15,
                    label: 'Company Address',
                    type: 'text',
                    placeholder: 'address Line 1',
                    styles: {
                        paddingTop: 10,
                        color: 'green'
                    },
                    response: undefined,
                    required: true,
                    row: 1
                },
                { 
                    id: 16,
                    label: 'Zip Code',
                    type: 'number',
                    placeholder: 'zip code',
                    styles: {
                        paddingTop: 10,
                        color: 'green'
                    },
                    response: undefined,
                    required: true,
                    row: 0.33
                },
                { 
                    id: 17,
                    label: 'State',
                    type: 'text',
                    placeholder: 'State Name',
                    styles: {
                        paddingTop: 10,
                        color: 'green'
                    },
                    response: undefined,
                    required: true,
                    row: 0.33
                },
                { 
                    id: 18,
                    label: 'Country',
                    type: 'text',
                    placeholder: 'Country Name',
                    styles: {
                        paddingTop: 10,
                        color: 'green'
                    },
                    response: undefined,
                    required: true,
                    row: 0.33
                }
            ]
        },
        { 
            sectionName: 'Extra',
            show: true,
            inputs: [
                { 
                    id: 19,
                    label: 'Do you want to be contacted by us?',
                    type: 'radio',
                    value: ['No, go away', 'Please, I need someone to talk to'],
                    styles: {
                        paddingTop: 10,
                        color: 'green'
                    },
                    response: undefined,
                    required: true,
                    row: 1
                },
                { 
                    id: 20,
                    label: 'Any personal Preference?',
                    type: 'textarea',
                    placeholder: 'zip code',
                    styles: {
                        paddingTop: 10
                    },
                    response: undefined,
                    required: true,
                    row: 1,
                    numberOfLine: 5
                }
            ]
        }

    ]
};
