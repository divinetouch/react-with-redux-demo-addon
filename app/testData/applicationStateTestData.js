export const testData = { 
    hasError: false,
    isAdmin: false,
    errorMessage: undefined,
    currentRegType: undefined,
    previousRegType: undefined,
    currentRegCategory: undefined,
    previousRegCategory: undefined,
    firstName: undefined,
    lastName: undefined,
    index: undefined,
    applicationIsReady: false,
    profiles: []
};
