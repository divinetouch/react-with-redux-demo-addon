let generateId = () => (Math.random() * 10000000000).toFixed(0);

export const testData = { 
    sections: [
        { 
            sectionName: 'Registration Type 1',
            description: 'This is the sessions before conference officially start',
            show: true,
            columnsName: ['', 'Name', 'Fee'],
            regCategoryId: generateId(),
            inputs: [
                { 
                    name: 'Type 1',
                    description: 'This is two hours session',
                    type: 'checkbox',
                    value: [],
                    response: undefined,
                    required: false,
                    qantity: 1000,
                    price: 50,
                    id: '00001',
                    row: 1
                },
                { 
                    name: 'Type 2',
                    description: 'This is one hour session',
                    type: 'checkbox',
                    value: [],
                    response: undefined,
                    required: false,
                    qantity: 1000,
                    price: 10,
                    id: '00002',
                    row: 1
                },
                { 
                    name: 'Type 3',
                    description: 'This is one hour session',
                    type: 'checkbox',
                    value: [],
                    response: undefined,
                    required: false,
                    qantity: 1000,
                    price: 500,
                    id: '00003',
                    row: 1
                },
                { 
                    name: 'Type 4',
                    description: 'This is one hour session',
                    type: 'checkbox',
                    value: [],
                    response: undefined,
                    required: false,
                    qantity: 1000,
                    price: 50,
                    id: '00004',
                    row: 1
                },
                { 
                    name: 'Type 5',
                    description: 'This is one hour session',
                    type: 'checkbox',
                    value: [],
                    response: undefined,
                    required: false,
                    qantity: 1000,
                    price: 50,
                    id: '00005',
                    row: 1
                }

            ]
        },
        { 
            sectionName: 'Registraion Type 2',
            description: 'This is the sessions before conference officially start',
            show: true,
            columnsName: ['', 'Name', 'Fee'],
            regCategoryId: generateId(),
            inputs: [
                { 
                    name: 'Type 6',
                    description: 'This is two hours session',
                    type: 'checkbox',
                    value: [],
                    response: undefined,
                    required: false,
                    qantity: 1000,
                    price: 50,
                    id: '00006',
                    row: 1
                },
                { 
                    name: 'Type 7',
                    description: 'This is one hour session',
                    type: 'checkbox',
                    value: [],
                    response: undefined,
                    required: false,
                    qantity: 1000,
                    price: 10,
                    id: '00007',
                    row: 1
                },
                { 
                    name: 'Type 8',
                    description: 'This is one hour session',
                    type: 'checkbox',
                    value: [],
                    response: undefined,
                    required: false,
                    qantity: 1000,
                    price: 500,
                    id: '00008',
                    row: 1
                },
                { 
                    name: 'Type 9',
                    description: 'This is one hour session',
                    type: 'checkbox',
                    value: [],
                    response: undefined,
                    required: false,
                    qantity: 1000,
                    price: 50,
                    id: '00009',
                    row: 1
                },
                { 
                    name: 'Type 10',
                    description: 'This is one hour session',
                    type: 'checkbox',
                    value: [],
                    response: undefined,
                    required: false,
                    qantity: 1000,
                    price: 50,
                    id: '00010',
                    row: 1
                }

            ]
        }
    ]
};
