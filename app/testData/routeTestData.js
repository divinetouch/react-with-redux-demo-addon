export const testData = { 
    paths: [
        { 
            active: false,
            path: 'Registration',
            breadCrumb: true,
            disabled: false
        },
        { 
            active: false,
            path: 'Profile',
            breadCrumb: true,
            disabled: true
        },
        { 
            active: false,
            path: 'Survey',
            breadCrumb: true,
            disabled: true
        },
        { 
            active: false,
            path: 'Recap',
            breadCrumb: true,
            disabled: true
        },
        { 
            active: false,
            path: 'Checkout',
            breadCrumb: true,
            disabled: true
        },
        { 
            active: false,
            path: 'Confirmation'
        },
        { 
            active: false,
            path: 'Help'
        },
        { 
            active: false,
            path: 'About'
        },
        { 
            active: false,
            path: 'Contact'
        },
        { 
            active: false,
            path: 'Home'
        }
    ]
};
