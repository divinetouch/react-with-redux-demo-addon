
let generateId = () => (Math.random() * 10000000).toFixed(0);

export const testData = { 
    inputs: [
        { 
            id: generateId(),
            label: 'Emeegency Contact Name',
            type: 'text',
            placeholder: 'Full name',
            styles: {
                paddingTop: 10,
                color: 'black'
            },
            response: undefined,
            required: true,
            row: 1
        },
        { 
            id: generateId(),
            label: 'Emergency Phone Number',
            type: 'text',
            placeholder: '###-###-####',
            styles: {
                paddingTop: 10,
                color: 'black'
            },
            response: undefined,
            required: false,
            row: 0.5
        },
        { 
            id: generateId(),
            label: 'Emergency Address',
            type: 'text',
            placeholder: 'Address',
            styles: {
                paddingTop: 10,
                color: 'green'
            },
            response: undefined,
            required: true,
            row: 1
        },
        {
            value: ['', 'Facebook','Google','Friend','Newspaper'],
            label: 'How did you hear about us?',
            type: 'select',
            id: generateId(),
            styles: { 
                paddingTop: 10,
                color: 'blue',
                fontSize: 15
            },
            response: '', //default value, the view should just response to this preset value
            required: true,
            row: 1
        },
        {
            value: ['Facebook','Google','Friend','Newspaper'],
            label: 'How did you hear about us?',
            type: 'select',
            id: generateId(),
            styles: { 
                paddingTop: 10,
                color: 'red',
                fontWeigth: 'bold'
            },
            response: 'Google', //default value
            required: true,
            row: 1
        },
        { 
            id: generateId(),
            type: 'radio',
            value: ['Yes, please! I am really hungry!!!!', 'Go away, I am full'],
            label: 'Do you want a free meal?',
            styles: { 
                paddingTop: 10,
                color: 'black',
                fontWeigth: 'bold'
            },
            response: undefined,
            required: true,
            row: 1
        },
        { 
            id: generateId(),
            type: 'checkbox',
            value: [],
            label: 'I want a free five stars hotel room',
            styles: { 
                paddingTop: 10,
                fontWeigth: 'bold'
            },
            response: undefined,
            required: true,
            row: 1
        },
        {
            id: generateId(),
            type: 'textarea',
            value: [],
            label: 'Please tell us how can we improve your registration experience',
            styles: { 
                paddingTop: 10
            },
            response: undefined,
            required: true,
            row: 1,
            numberOfLine: 5
        }
    ]
};
