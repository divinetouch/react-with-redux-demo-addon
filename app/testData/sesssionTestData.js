let generateId = () => (Math.random() * 10000000000).toFixed(0);

export const testData = { 
    sections: [
        { 
            sectionName: 'Pre Conference',
            description: 'This is the sessions before conference officially start',
            show: true,
            columnsName: ['Qty', 'Name', 'Fee'],
            inputs: [
                { 
                    id: 1,
                    name: 'Saturday Session',
                    description: 'This is two hours session',
                    type: 'select',
                    value: [0,1,2,3,4,5,6],
                    response: undefined,
                    required: false,
                    qantity: 1000,
                    price: 50,
                    regType: '00001',
                    row: 1
                },
                { 
                    id: 2,
                    name: 'Sunday Session',
                    description: 'This is one hour session',
                    type: 'checkbox',
                    value: [],
                    response: undefined,
                    required: false,
                    qantity: 1000,
                    price: 10,
                    regType: '00002',
                    row: 1
                },
                { 
                    id: 3,
                    name: 'Friday Session',
                    description: 'This is one hour session',
                    type: 'checkbox',
                    value: [],
                    response: undefined,
                    required: false,
                    qantity: 1000,
                    price: 500,
                    regType: '00001',
                    row: 1
                },
                { 
                    id: 4,
                    name: 'Saturday Night Session',
                    description: 'This is one hour session',
                    type: 'checkbox',
                    value: [],
                    response: undefined,
                    required: false,
                    qantity: 1000,
                    price: 50,
                    regType: '00002',
                    row: 1
                },
                { 
                    id: 5,
                    name: 'Sunday Night Session',
                    description: 'This is one hour session',
                    type: 'select',
                    value: [0,10,20,30,40],
                    response: undefined,
                    required: false,
                    qantity: 1000,
                    price: 50,
                    regType: '00002',
                    row: 1
                }

            ]
        },
        { 
            sectionName: 'After Conference',
            description: 'This is the sessions before conference officially start',
            show: true,
            columnsName: ['Qty', 'Name', 'Fee'],
            inputs: [
                { 
                    id: 6,
                    name: 'Saturday Session',
                    description: 'This is two hours session',
                    type: 'select',
                    value: [0,1,2,3,4,5,6],
                    response: undefined,
                    required: false,
                    qantity: 1000,
                    price: 50,
                    regType: '00001',
                    row: 1
                },
                { 
                    id: 7,
                    name: 'Sunday Session',
                    description: 'This is one hour session',
                    type: 'checkbox',
                    value: [],
                    response: undefined,
                    required: false,
                    qantity: 1000,
                    price: 10,
                    regType: '00002',
                    row: 1
                },
                { 
                    id: 8,
                    name: 'Friday Session',
                    description: 'This is one hour session',
                    type: 'checkbox',
                    value: [],
                    response: undefined,
                    required: false,
                    qantity: 1000,
                    price: 500,
                    regType: '00001',
                    row: 1
                },
                { 
                    id: 9,
                    name: 'Saturday Night Session',
                    description: 'This is one hour session',
                    type: 'checkbox',
                    value: [],
                    response: undefined,
                    required: false,
                    qantity: 1000,
                    price: 50,
                    regType: '00002',
                    row: 1
                },
                { 
                    id: 10,
                    name: 'Sunday Night Session',
                    description: 'This is one hour session',
                    type: 'checkbox',
                    value: [],
                    response: undefined,
                    required: false,
                    qantity: 1000,
                    price: 50,
                    regType: '00002',
                    row: 1
                }

            ]
        }
    ]
};
