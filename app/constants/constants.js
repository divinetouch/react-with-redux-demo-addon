export const ERROR = { 
    REGTYPE_ERROR: 'Please select a registration type below',
    GENERIC_ERROR: 'Please make sure that all the required fields are not empty',
    REQUIRED_ERROR: 'This field is required'
};

export const STATE_NAME = { 
    CART: 'cart',
    COUNTER: 'counter',
    PROFILE_INPUTS: 'profileInputs',
    SURVEY_INPUTS: 'surveyInputs',
    SESSION_MERCHANDIZE: 'sessionMerchandize',
    REGTYPE_MERCHANDIZE: 'regTypeMerchandize',
    APPLICATION_STATE: 'applicationState',
    APPLICATION_ROUTE: 'applicationRoute'
};
